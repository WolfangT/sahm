#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Copyright 2014 Wolfang Torres <wolfang.torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
__NOMBRE__ = "LibSA"
__VERSION__ = 1.5
import logging
__registro__ = logging.getLogger(__NOMBRE__)
__registro__.info('Iniciando {} {}'.format(__NOMBRE__, __VERSION__))

import sys, os, traceback, platform
import time, datetime

from xml.etree.ElementTree import Element, ElementTree, tostring, ProcessingInstruction as PI
from tkinter import Text, messagebox, Tk, Toplevel, ttk

import pygubu

import Libs.BD_XML as bd

formato_fecha_usuario = "%d-%m-%y %I:%M %p"
formato_fecha_iso = "%Y-%m-%d %H:%M:%S"
formato_fecha = "%d-%m-%Y"
formato_hora = "%H:%M:%S"
formato_fecha_hora = formato_fecha  + ' ' + formato_hora

class Error(Exception):
    """Error Base para el Modulo

    Atibutos:
        arg - Contenido del error
        obj - Objeto que creo el error

    Metodos:
        mensaje - genera un messagebox.showwarning
        con texto aclaratorio del error

    """

    title = None
    message = None
    detail = None
    parent = None

    def __init__(self, arg, obj=None):
        self.arg = arg
        self.title = """Error"""
        self.message = self.arg
        self.obj = obj
        self.detail = ''

    def mensaje(self):
        try:
            self.parent = self.obj.master
        except AttributeError:
            self.parent = None

        o = {
        'title':self.title,
        'message':self.message,
        'detail':self.detail,
        'parent':self.parent
        }

        __registro__.info('{title}: {message}'.format(**o))
        messagebox.showwarning(**o)

    def __str__(self):
        return self.message


class ErrorEscritura(Error):
    """Error producido cuando un campo ha sido escrito con un tipo de valor equibocado

    Atributos:
        campo - Nombre de la variable que contiene el valor
        esp - Tipo de valor esperado para el campo
        valor - Valor introducido(si es posible obtenerlo)
        tipo - Clase del valor introducido(si es posible obtenerlo)
        nota - Comentario informativo acerca del Error(Opcional)
    """

    def __init__(self, campo, esp, valor=None, nota=None, obj=None):
        self.campo = campo
        self.esp = esp
        self.valor = valor
        self.tipo = tipo = {str:'Texto', int:'Numero', float:'Decimal', bool:'Cierto/Falso', type(None):'Nulo'}[type(valor)]
        self.nota = nota
        self.obj = obj

        self.title = """Error de Escritura en <{campo}>""".format(**locals())
        self.message = """Se esperaba obtener <{esp}> pero se obtubo <{tipo}>""".format(**locals())
        self.detail = self.nota

class ErrorFormato(Error):
    """Error producido cuando un campo ha sido mal escrito, no sigiendo un formato

    Atributos:
        campo - Nombre de la variable que contiene el valor
        formato - Tipo de valor esperado para el campo
        valor - Valor introducido
        nota - Comentario informativo acerca del Error(Opcional)
    """

    def __init__(self, campo, formato, valor, nota=None, obj=None):
        self.campo = campo
        self.formato = formato
        self.valor = valor
        self.nota = nota
        self.obj = obj

        self.title = """Error de Formato en <{campo}>""".format(**locals())
        self.message = """<{valor}> no sigue el formato <{formato}>""".format(**locals())
        self.detail = self.nota


class ErrorValor(Error):
    """Error producido cuando un campo contiene un valor sin sentido en el contexto dado

    Atributos:
        campo - Nombre de la variable que contiene el valor
        con - Contexto en el que se introduce el valor
        valor - Valor introducido
        nota - Comentario informativo acerca del Error(Opcional)
    """

    def __init__(self, campo, con, valor, nota=None, obj=None):
        self.campo = campo
        self.con = con
        self.valor = valor
        self.nota = nota
        self.obj = obj

        self.title = """Error de Valor en <{campo}>""".format(**locals())
        self.message = """Debido a que <{con}>, <{valor}> no tiene sentido""".format(**locals())
        self.detail = self.nota


def _seguro(funcion):
    __registro__.debug('Creando wraper seguro para función {}'.format(funcion))
    det = "Verifique el archivo de registro (registro.log) para mas detalles"

    def ejecucion(*arg, **kwarg):
        __registro__.debug('Iniciando una ejecucion segura {}'.format(funcion))
        try:
            return funcion(*arg, **kwarg)

        except Error as err:
            __registro__.debug('Se atrapo al error de libsa {err}'.format(**locals()))

            err.mensaje()

            return False

        except IOError as err:
            __registro__.debug('Se atrapo al error de IO {err}'.format(**locals()))
            error = traceback.format_exc()
            __registro__.error(error)

            msj = '''Ocurrio un error al intentar leer o escribir un archivo.'''
            det = '''Verifique que todos los archivos del sistema esten en su lugar
y que tiene permisos nesesarios para usarlos.
''' + detalles
            messagebox.showerror(title='Error Archivo', icon="error", message=msj, detail=det)

            return False


        except Exception as err:
            __registro__.debug('Se atrapo al error {err}'.format(**locals()))
            error = traceback.format_exc()
            __registro__.error(error)

            msj = '''Ha ocurrido un error Desconocido:'''
            det = err
            messagebox.showerror(title='Error Desconocido', icon="error", message=msj, detail=det)

            return False

        finally:
            __registro__.debug('Cerrando la ejecucion segura {}'.format(funcion))

    return ejecucion

@_seguro
def _ejecutar(funciones):
    for fun in funciones:
        if fun():
            continue
        else:
            return False
    else:
        return True


class Ventana:

    carpetas_recursos = []

    nombre_objeto_ventana = '__ventana__'

    objeto_menu = '__menu__'

    def __init__(self, master, archivo_gui, menu=False):
        self.master = master
        self.archivo_gui = archivo_gui

        self._crear_gui(archivo_gui)

        if menu:
            self._crear_menu()

        self.__gui__.connect_callbacks(self)

        self.mapeo_text_variable


    def _crear_gui(self, GUI):
        #Contrustor
        self.__gui__ = pygubu.Builder()

        for carpeta in self.carpetas_recursos:
            self.__gui__.add_resource_path(carpeta)

        self.__gui__.add_from_file(os.path.join(self.carpeta_guis, GUI+'.ui'))

        #Widgets
        self.__vp__ = self.__gui__.get_object(self.nombre_objeto_ventana, self.master)

        self.master.rowconfigure(0, weight=1)
        self.master.columnconfigure(0, weight=1)


    def _crear_menu(self):
        self.menu = self.__gui__.get_object(self.objeto_menu)

        self.master.configure(menu=self.menu)

    @property
    def mapeo_text_variable(self):
        '''Regresa un Dicionario con los tk.Text y sus variables.'''
        mapa = {}

        for nombre, widget in self.__gui__.objects.items():
            if widget.class_ == Text:
                variable = self.__gui__.create_variable(nombre)
                mapa[widget.widget] = variable
                widget.widget.bind("<FocusOut>", self.obtener_text_variable)

        return mapa


    @property
    def datos(self):
        '''Regresa un mapa de los nombre de variable y su respectivo valor'''
        datos = {}

        for var, tkvar in self.__gui__.tkvariables.items():
            try:
                datos[var] = tkvar.get()
            except ValueError:
                msj = """Tipo de dato erroneo guadado en la variable {}
es la variable {} de tipo {}""".format(var, tkvar, type(tkvar))
                datos[var] = None

        return datos

    def _obtener_nombre(self, objeto):
        objeto = self.__vp__.nametowidget(objeto)
        for nombre, widget in self.__gui__.objects.items():
            if str(widget.widget) == str(objeto):
                return nombre
        else:
            return None


    def _cambiar_estado(self, widgets=('Entry','Combobox','Radiobutton','Checkbutton', 'Text', 'Spinbox'), estado='disabled'):
        for w in self.__gui__.objects:
            for t in widgets:
                if t in str(self.__gui__.objects[w]): self.__gui__.get_object(w)['state'] = estado


    def _cambiar_texto(self, widgets, texto):
        for (w, t) in zip(widgets, texto):
            self.__gui__.get_object(w)['text']=t


    def colocar_text_variable(self):
        '''Escribe a un widget tk.text el contenido de una StringVar
         segun el mapeo de crear_mapa_text()'''
        m = self.mapeo_text_variable
        for widget in m:
            t = m[widget].get()
            s = False
            if widget['state'] == 'disabled':
                widget['state'] = 'normal'
                s = True
            widget.delete(1.0, 'end')
            widget.insert(1.0, t)
            if s:
                widget['state'] = 'disabled'


    #Bindings
    def obtener_text_variable(self, evento):
        '''Asigna el contenido de un tk.text a una StringVar
        segun el mapeo de crear_mapa_text()'''
        w = evento.widget
        self.mapeo_text_variable[w].set(limpiar_texto(w.get(0.0, 'end')))


class Base_Entrada:

    def __init__(self, **funciones):

        self.funciones_guardar = list(funciones.pop('guardar', []))

        self.funciones_cancelar = list(funciones.pop('cancelar', []))

        self.funciones_reiniciar = list(funciones.pop('reiniciar', []))

        self.funciones_limpiar = list(funciones.pop('limpiar', []))

        self.funciones_selecionar = list(funciones.pop('selecionar', []))

        self.funciones_buscar = list(funciones.pop('buscar', []))

        self.funciones_borrar = list(funciones.pop('borrar', []))

        self.funciones_cambio_registro = list(funciones.pop('cambio_registro', []))

        self.funciones_activar = list(funciones.pop('activar', []))

        self.funciones_desactivar = list(funciones.pop('descartivar', []))

    @_seguro
    def sincronisar(self, dir='tabla', reg=0, tabla=None, *opciones):
        ''' sincronisar (dir = ( tabla / formulario ), reg=0, tabla=<objeto Tabla>)
        Sincronisa la tabla/formulario con los contenidos del otro.

        Opciones:
            -no_pri: ignora la columna si es primaria
            -none_a_str: comvierte los NoneType a str()
            -fecha: comvierte los time.struc_time a formato
        '''

        if tabla is None: tabla = self.tabla

        no_pri = 'no_pri' in opciones
        none_a_str = 'none_a_str' in opciones
        fecha = 'fecha' in opciones

        if dir == 'tabla':
            while True:
                try:
                    for col in tabla[reg]:
                        if no_pri and tabla.ver_columna(col, 'llave') == 'PRI': continue

                        try:
                            tabla[reg][col] = self.__gui__.get_variable(col).get()
                        except ValueError:
                            msj = """Seguramente Escribio mal un numero,
verifique que uso '.' enves de ','
para identificar decimales"""
                            raise ErrorEscritura(var, tabla.ver_columna(var, 'tipo'), None, msj)

                except IndexError:
                    tabla += {}
                else:
                    break

        elif dir == 'formulario':
            while True:
                try:
                    for col in tabla[reg]:
                        d = tabla[reg][col]

                        if no_pri and tabla.ver_columna(col, 'llave') == 'PRI': continue
                        if none_a_str and d is None: d = ''
                        if isinstance(d, time.struct_time) and fecha: d = time.strftime(formato_fecha_hora, d)

                        self.__gui__.get_variable(col).set(d)

                    self.colocar_text_variable()
                except IndexError:
                    tabla += {}
                else:
                    break

    def _validar_todo(self):
        '''Va por todos los objetos, verifica si estan en la lista de validables y los valida'''
        for nombre, widget in self.__gui__.objects.items():
            if widget.class_ in (ttk.Entry, ttk.Combobox):
                try:
                    r = widget.widget.validate()
                except Exception:
                    __registro__.warning('No se pudo validar {}'.format(nombre))
                else:
                    if not r: return False
        else:
            return True

    #Funciones Comunes
    @_seguro
    def reiniciar(self, evento=None):
        _ejecutar(self.funciones_reiniciar)

    @_seguro
    def limpiar(self, evento=None):
        for campo, defecto in [(col['campo'], col['defecto']) for col in self.tabla._col]:
            self.__gui__.create_variable(campo).set(defecto if not defecto is None else '')
        _ejecutar(self.funciones_limpiar)


    #Validaciones Basicas
    def _validar_fecha(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == "":
            return True

        try:
            datetime.datetime.strptime(P, formato_fecha)

        except ValueError:
            msj = ("""Verifique que no escribio mes 13 o febrero 30
Intente escribir '-' enves de '/'""")
            ErrorFormato(nombre, formato_fecha, P, msj, obj=self).mensaje()
            return False

        return True

    def _validar_hora(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == "":
            return True

        try:
            datetime.datetime.strptime(P, formato_hora)

        except ValueError:
            msj = ("""Verifique que no escribio mes 13 o febrero 30
Intente escribir '-' enves de '/'""")

            ErrorFormato(nombre, formato_hora, P, msj, obj=self).mensaje()
            return False

        return True

    def _validar_decimal(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        dato = P.split('.')

        if not len(dato) in range(1,3):
           return False

        if not sum([1 if (v.isdecimal() or v == '') else 0 for v in dato]) == len(dato):
            return False

        if (not len(dato) == 1) and (sum([1 if v == '' else 0 for v in dato]) == len(dato)):
            msj = ("""verifique que uso '.' enves de ',' para identificar decimales""")
            ErrorEscritura(nombre, 'Decimal', P, msj, obj=self).mensaje()
            return False

        return True

    def _validar_numero(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == "":
            return True

        if not P.isdecimal():
            msj = ("""Seguramente Escribio mal un numero,
verifique que escribio '0' en ves de dejarlo vacio""")
            ErrorEscritura(nombre, 'Numero', P, msj, obj=self).mensaje()
            return False

        return True

    def _validar_texto(self, d,  i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == "":
            return True

        if not len(P) in range(1,51):
            con = ("""El texto no puede ser mayor a 50 caracteres""")
            val = ("""{}... ({} caracteres)""".format(P[:10], len(P)))
            ErrorValor(nombre, con, val, obj=self).mensaje()
            return False

        return True

    def _validar_ci_tipo(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == '':
            return True
            #con = '{} es obligatorio'.format(nombre)
            #ErrorValor(nombre, con, P, obj=self).mensaje()
            #return False

        if not P.upper() in ('V', 'E'):
            con = '{} solo acepta {}'.format(nombre, ('V', 'E'))
            nota = 'Escoja una de las opciones dadas'
            ErrorValor(nombre, con, P, nota, obj=self).mensaje()
            return False

        return True


    def _validar_ci_numero(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == '':
            return True
            #con = '{} es obligatorio'.format(nombre)
            #ErrorValor(nombre, con, P, obj=self).mensaje()
            #return False

        if not P.isdigit():
            esp = 'Numero'
            ErrorEscritura(nombre, esp, P, obj=self).mensaje()
            return False

        if not len(P) in range(1,9):
            con = 'La C.I. no puede tener mas de 8 digitos'
            ErrorValor(nombre, con, P, obj=self).mensaje()
            return False

        if self.tabla.ejecutar_proc('General.CHQ_EXISTENCIA', 'Pacientes.Personas', 'ci_numero', P, 0)[3]:
            con = 'La C.I. <{}> ya existe'.format(P)
            nota = 'Intente iniciar una consulta en ves'
            ErrorValor(nombre, con, P, nota, obj=self).mensaje()
            return False

        return True

    def _validar_telefono(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == '':
            return True

        if not len(P) in range(1,8):
            con = ("""El texto no puede ser mayor a 7 caracteres""")
            val = ("""{}... ({} caracteres)""".format(P[:7], len(P)))
            ErrorValor(nombre, con, val, obj=self).mensaje()
            return False

        if not P.isdecimal():
            msj = ("""Seguramente Escribio mal un numero,
verifique que escribio '0' en ves de dejarlo vacio""")
            ErrorEscritura(nombre, 'Numero', P, msj, obj=self).mensaje()
            return False

        return True

    def _validar_telefono_codigo(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == '':
            return True

        if not len(P)in range(0,5):
            con = ("""El texto no puede ser mayor a 4 caracteres""")
            val = ("""{}... ({} caracteres)""".format(P[:4], len(P)))
            ErrorValor(nombre, con, val, obj=self).mensaje()
            return False

        if not P.isdecimal():
            msj = ("""Seguramente Escribio mal un numero,
verifique que escribio '0' en ves de dejarlo vacio""")
            ErrorEscritura(nombre, 'Numero', P, msj, obj=self).mensaje()
            return False

        return True


class Formulario(Base_Entrada):
    nombre_objeto_formulario = '__formulario__'

    def __init__(self, tabla, metodo='actualisar', **funciones):
        self.tabla = tabla
        self.metodo = metodo

        Base_Entrada.__init__(self, **funciones)

        if self.metodo == 'actualisar':
            try:
                self.tabla[0] = {}
            except IndexError:
                self.tabla += {}

    #Funciones
    @_seguro
    def cancelar(self, evento=None):
        if not _ejecutar(self.funciones_cancelar):
            self.master.destroy()

    @_seguro
    def guardar(self, evento=None):
        self._validar_todo()
        if self.metodo == 'agregar':
            self._agregar()
        elif self.metodo == 'actualisar':
            self._actualisar()

        return _ejecutar(self.funciones_guardar)


    #Metodos
    @_seguro
    def _agregar(self, evento=None):
        self.tabla += self.datos
        return True

    @_seguro
    def _actualisar(self, evento=None):
        self.tabla[0] = self.datos
        return True


class Bucle(Base_Entrada):
    nombre_objeto_bucle = '__bucle__'

    def __init__(self, tabla, **funciones):
        self.tabla = tabla

        Base_Entrada.__init__(self, **funciones)

        self.__pos__ = self.__gui__.create_variable('int:__pos__')
        self.__tot__ = self.__gui__.create_variable('int:__tot__')
        self.__pos__.set(0)
        self.__tot__.set(0)

    #Metodos
    def _activar(self):
        _ejecutar(self.funciones_activar)
        self.__pos__.set(1 if len(self.tabla) >= 1 else 0)
        self.__tot__.set(len(self.tabla))
        self.sincronisar('formulario', self.__pos__.get()-1, self.tabla,'fecha', 'none_a_str')
        _ejecutar(self.funciones_cambio_registro)

    def _desactivar(self):
        _ejecutar(self.funciones_desactivar)
        self.__pos__.set(0)
        self.__tot__.set(0)
        _ejecutar(self.funciones_cambio_registro)
        self._cambiar_estado()

    def cambiar_registro(self, numero_registro):
        self.__pos__.set(numero_registro + 1)
        self.sincronisar('formulario', numero_registro, None, 'fecha', 'none_a_str')
        _ejecutar(self.funciones_cambio_registro)

    #Funciones
    @_seguro
    def registro_ant(self, evento=None):
        p = self.__pos__.get() -1
        t = self.__tot__.get() -1
        if p > 0:
            p -= 1
        else:
            p = t
        self.sincronisar('formulario', p, None, 'fecha', 'none_a_str')
        self.__pos__.set(p+1)
        _ejecutar(self.funciones_cambio_registro)

    @_seguro
    def registro_sig(self, evento=None):
        p = self.__pos__.get() -1
        t = self.__tot__.get() -1
        if p < t:
            p += 1
        else:
            p = 0
        self.sincronisar('formulario', p, None, 'fecha', 'none_a_str')
        self.__pos__.set(p+1)
        _ejecutar(self.funciones_cambio_registro)

    @_seguro
    def selecionar(self, evento=None):
        _ejecutar(self.funciones_selecionar)
        self._desactivar()

    @_seguro
    def guardar(self, evento=None):
        numero_registro = (self.__pos__.get() - 1)
        self.tabla[numero_registro] = self.datos
        _ejecutar(self.funciones_guardar)

    @_seguro
    def borrar(self, evento=None):
        numero_registro = (self.__pos__.get() - 1)
        del self.tabla[numero_registro]
        self.__tot__.set(len(self.tabla))
        _ejecutar(self.funciones_borrar)
        self.registro_ant()

    @_seguro
    def buscar(self, evento=None):
        _ejecutar(self.funciones_buscar)

        if self.tabla:
            self._activar()
        _ejecutar(self.funciones_cambio_registro)


class Tabla:
    nombre_objeto_tabla = '__tabla__'

    def __init__(self, tabla):
        self.tabla = tabla
        self.__tabla__ = self.__gui__.get_object(self.nombre_objeto_tabla)

        self.lista_filas = []

        self.actualisar_tabla()

    @_seguro
    def agregar_valores(self):
        self.tabla += self.datos
        self.actualisar_tabla()

    @_seguro
    def eliminar_valores(self):
        for id in self.__tabla__.selection():
            reg = self.lista_filas.index(id)
            del self.tabla[reg]

        self.actualisar_tabla()

    @_seguro
    def actualisar_tabla(self):
        for i in self.lista_filas:
            self.__tabla__.delete(i)
        self.lista_filas = []

        for reg in self.tabla:
            valores = []
            for col in self.tabla.columnas:
                if isinstance(reg[col], time.struct_time):
                    valores.append(time.strftime(formato_fecha_hora, reg[col]))
                else:
                    valores.append(reg[col])

            id = self.__tabla__.insert('', 'end', values=valores, tags='fila')
            self.lista_filas.append(id)

    @_seguro
    def seleccion(self):
        reg = self.__tabla__.selection()
        filas = []
        for fil in reg:
            n = self.lista_filas.index(fil)
            filas.append(self.tabla[n])
        return tuple(filas)


class Reporte:

    clausula_xml = '''version="{version}" encoding="{encoding}"'''
    clausula_estilo = '''type="{type}" href="{href}"'''
    id_estilo = '''stylesheet'''
    tag = '''reporte'''
    encoding = 'utf-8' if platform.system() == 'Linux' else 'ISO-8859-1'
    version = '1.0'

    hoja_estilo = None
    tipo_hoja_estilo = None

    def __init__(self, *tablas, **configuracion):
        self.tablas = tablas

        self.hoja_estilo = configuracion.get('hoja_estilo')

        tipo = 'xsl'
        if '.' in self.hoja_estilo:
            hoja, tipo = self.hoja_estilo.split('.')
            self.tipo_hoja_estilo = tipo

        self.tipo_hoja_estilo = configuracion.get('tipo_hoja_estilo', tipo)


    def reporte_xml_estilo(self):
        nodo_root = datos_xml(*self.tablas)
        nodo_root.tag = self.tag
        nodo_root.set('fecha' , time.strftime(formato_fecha))

        nodo_xml = PI('xml', self.clausula_xml.format(version=self.version, encoding=self.encoding))

        assert  self.hoja_estilo, 'Falsa hoja de estilo'

        #href = os.path.join(self.carpeta_reportes, self.hoja_estilo)
        #href = 'FILE://' + href
        href = self.hoja_estilo

        tipo = self.tipo_hoja_estilo
        type = '''text/{tipo}'''.format(tipo=tipo)
        nodo_estilo = PI('xml-stylesheet', self.clausula_estilo.format(type=type, href=href))

        xml = (nodo_xml, nodo_estilo, nodo_root)
        return '\n'.join( [tostring(nodo, 'unicode') for nodo in xml] )

def sincronisar_varibles(padre, hijo, variables):
    '''Copia y Pega los valores de las varibles de los objetos'''
    for var in variables:
        hijo.__gui__.get_variable(var).set(padre.__gui__.get_variable(var).get())


def limpiar_texto(texto):
    texto.replace('\t','')
    while texto[-1:] == '\n':
        texto = texto[:-1]
    return texto


def calc_meses(dias):
    meses = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    mes = 0

    while dias >= meses[mes]:
        dias -= meses[mes]
        mes += 1

    return mes, dias


def calc_dias(mes):
    meses = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    return sum(meses[:mes])


def timedelta_a_iso(timedelta):
    '''convierte un timedate.timedelta a formato iso: YYYY-MM-DD HH:mm:ss, sin extra ceros'''

    formato = "{YYYY}-{MM}-{DD} {HH}:{mm}:{ss}"

    mm, ss = divmod(timedelta.seconds, 60)
    HH, mm = divmod(mm, 60)
    YYYY, DD = divmod(timedelta.days, 365)
    MM, DD = calc_meses(DD)

    return formato.format(**locals())


def timedelta_a_fecha(timedelta):
    '''convierte un timedate.timedelta a formato iso: YYYY-MM-DD, sin extra ceros'''

    formato = "{YYYY}-{MM}-{DD}"
    YYYY, DD = divmod(timedelta.days, 365)
    MM, DD = calc_meses(DD)
    return formato.format(**locals())


def timedelta_a_duracion(timedelta):
    '''convierte un timedate.timedelta a formato legible: YYYY años, DD dias, hh horas'''

    formato = '''{YYYY}-{MM}-{DD} {HH}:{mm}:{ss}'''


    mm, ss = divmod(timedelta.seconds, 60)
    hh, mm = divmod(mm, 60)
    YYYY, DD = divmod(timedelta.days, 365)
    MM, DD = calc_meses(DD)

    formato = (('años', YYYY), ('meses', MM), ('dias', DD), ('horas', hh), ('minutos', mm), ('segundos', ss))

    return ' '.join(["{} {}".format(valor, texto) for texto, valor in formato if valor > 0])


def capitalisar_palabras(texto):
    l=[]

    for i in texto.split():
        l.append(i.capitalize())

    return ' '.join(l)


def convertir_fecha(tabla, campo, formato, obj=None):
    for fil in tabla:
        try:
            fil[campo] = time.strptime(fil[campo], formato)
        except ValueError:
            msj = """Intente escribir '-' enves de '/'"""
            raise ErrorFormato("{}, fila {}".format(campo, fil.pos), formato, fil[campo], msj, self, obj)


def datos_xml(*tablas, metadatos=False):
    """Devuelve un objeto Element con los datos de las tablas"""

    xml = Element('xml')
    for n in range(len(tablas)):
        tablas[n].exportar_xml(metadatos)
        xml.append(tablas[n].xml_arbol.getroot())

    return xml
