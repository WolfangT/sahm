import tkinter
from tkinter import ttk

class Scrolled_Frame(ttk.Frame):
    
    def __init__(self, master, **kw):
        ttk.Frame.__init__(self, master, **kw)
        
        self.canvas = tkinter.Canvas(self)
        self.frame = ttk.Frame(self.canvas)
        self.scrollbar = ttk.Scrollbar(self ,orient="vertical",
                                       command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.scrollbar.set)
        self.frame.bind("<Configure>",self.frame_callback)
        
        self.scrollbar.grid(row=0, column=1, sticky="ns")
        self.canvas.grid(row=0, column=0)
        self.canvas.create_window((0,0),window=self.frame,anchor='nw')        

    def frame_callback(self, event):
        self.canvas.configure(scrollregion=self.canvas.bbox("all"),
                              width=200,height=200)
        
def test():
    root = tkinter.Tk()  
    SF = Scrollable_Frame(root)
    SF.pack()
    for i in range(50):
        label = ttk.Label(SF.frame, text=str(i)).pack(side="top")    
    root.mainloop()

if __name__ == '__main__':
    test()
