import tkinter as tk
from tkinter import ttk

t=("Titulo A","Titulo B","Titulo C","Titulo D","Titulo E",
   "Titulo F","Titulo G","Titulo H","Titulo I","Titulo J")
a=("15", "15", "15", "15", "15", "15", "15", "15", "15", "15")

class Tabla:
    def __init__(self, rais, tab, col=5, titulos=t, anchos=a):
        self.var=[0]
        self.anchos=anchos
        self.col=col
        self.tab=tab
        self.listfil=[]
        self.titulos=titulos
        
        self.marco_T=ttk.Frame(rais, relief="sunken",  borderwidth=2)
        self.marco=ttk.Frame(rais, relief="sunken",  borderwidth=2)
        self.marco_T.pack(side="top")
               
        for i in range(self.col):
            self.titulo=ttk.Label(self.marco_T, text=titulos[i],
                                  width=anchos[i])
            self.titulo.pack(side="left")
        self.marco.pack(side="top", fill="y", expand="yes")
        
    def fila_nueva(self, f1=False, f2=True):        
        if self.var[0]>0 and f1:
            self.fila.guardar(f2)
        self.var[0] += 1
        self.fila=Fila(self.marco, self.var, self.col, self.anchos, self, self.tab)
        self.listfil.append(self.fila)
        
    def valores(self, x=None, n=0):
        val=[]
        for i in range(1, len(self.var)):
            for reg in range(len(self.var[i])):
                if self.titulos[reg]=="  CANT":
                    if not self.var[i][reg].isdigit():
                        self.var[i][reg]=1
            val.append(tuple(self.var[i]))
        val=tuple(val)
        if bool(x):
            val=list(val)
            for i in range(len(val)):
                val[i]=list(val[i])
                val[i].insert(n, x)
                val[i]=tuple(val[i])
            val=tuple(val)
        return val

    def borrar(self):
        for i in self.listfil:
            i.borrar(True)
            
    def escribir(self, val):
        for i in range(len(val)) :
            self.fila_nueva()
            self.fila.escribir(val[i])
        self.bloquear()    
                
    def guardar(self):
        for fila in self.listfil:
            fila.guardar()  

    def bloquear(self):
        for fila in self.listfil:
            fila.bloquear()

    def destruir(self):
        self.marco_T.destroy()
        self.marco.destroy()
    
class Fila:
    def __init__(self, rais, var, col, anchos, padre, tab):
        self.padre=padre
        self.col=col
        self.var=var
        self.rais=rais        
        self.g=False
        self.listvar=[]
        self.cel=[]
        self.marco=ttk.Frame(rais, relief="raised",  borderwidth=1)
        for i in range(self.col):
            self.celda=Celda(self.marco, self, self.listvar, anchos[i])          
        self.listvar[len(self.listvar)-1].entrada.bind("<Return>",
                                               lambda x: self.padre.fila_nueva(tab, tab))
        self.marco.pack(side="top", fill="x")
        self.listvar[0].entrada.focus()

    def borrar(self, f=False):
        self.valores=[]        
        if self.g:
            for i in self.listvar:
                self.valores.append(i.entrada.get())
            self.var.remove(self.valores)
            self.var[0] =self.var[0]-1
            self.marco.destroy()
        elif f:
            self.marco.destroy()
        self.padre.listfil.remove(self)

    def guardar(self, f=False):
        self.valores=[]
        for i in self.listvar:
            self.valores.append(i.entrada.get().upper())
            if f:
                i.entrada[("state")]="readonly"
        if bool(self.valores[0]):
            self.var.append(self.valores)
        self.g=True

    def bloquear(self):
        for i in self.listvar:
            i.entrada[("state")]="readonly"

    def escribir(self, valores):
        n=-1
        for i in self.listvar:
            n += 1
            i.entrada.delete(0,"end")
            if bool(valores[n]):                
                i.entrada.insert(0, valores[n])
            else:
                i.entrada.insert(0, "")

class Celda:
    def __init__(self, rais, padre, lista, ancho):
        self.padre=padre
        self.listcel=lista
        self.entrada=ttk.Entry(rais, width=ancho)
        self.listcel.append(self)
        self.entrada.pack(side="left")
        self.entrada.bind("<Return>", lambda x: self.celda_sig())

    def celda_sig(self):
        n=self.listcel.index(self)
        n=self.listcel[n+1].entrada
        n.focus()
