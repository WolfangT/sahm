#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  main.py
#
#  Copyright 2014 wolfang Torres <wolfang.torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import time, datetime, os, sys, subprocess

from datetime import datetime as fecha
from tkinter import ttk, messagebox, Tk, Toplevel

from Libs.libsa import Ventana, Formulario, Bucle, Reporte, _seguro
from Libs.libsa import formato_fecha_usuario, formato_fecha_iso, formato_fecha, formato_fecha_hora
from Libs.libsa import sincronisar_varibles, timedelta_a_iso, datos_xml
from Libs.libsa import Error, ErrorEscritura, ErrorValor, ErrorFormato

from Libs.BD_XML import Tabla
from Libs.Widgets.Calendario import Calendar as calendario

import formularios, bucles, mostradores, tablas


class Contenedor_Formulario(Ventana):

    archivo_gui = 'C_Formulario'

    def __init__(self, master, con, formulario, __estado__=None):
        self.master = master
        self.con = con.copy()

        Ventana.__init__(self, self.master, self.archivo_gui)

        if not __estado__ is None: self.__gui__.tkvariables['__estado__'] = __estado__
        self.__estado__ = self.__gui__.get_variable('__estado__')

        _marco = self.__gui__.get_object('_marco')

        self.formulario = formulario(_marco, self.con, self.__estado__)
        self.formulario.cancelar = self.cancelar
        self.__gui__.connect_callbacks(self.formulario)

    @_seguro
    def cancelar(self):
        Formulario.cancelar(self.formulario)
        self.master.destroy()
        return True

    def buscar(self):pass
    def reiniciar(self):pass
    def guardar(self):pass


class Contenedor_Bucle_Busqueda(Ventana):

    archivo_gui = 'C_Bucle_Busqueda'

    objeto_marco = '__marco__'

    objeto_tot = '__tot__'
    objeto_pos = '__pos__'

    __bucle__ = None
    __marco__ = None

    def __init__(self, master, con, bucle, __estado__=None):
        self.master = master
        self.con = con.copy()

        Ventana.__init__(self, self.master, self.archivo_gui)
        self.__pos__ = self.__gui__.get_variable(self.objeto_pos)
        self.__tot__ = self.__gui__.get_variable(self.objeto_tot)

        if not __estado__ is None:
            self.__gui__.tkvariables['__estado__'] = __estado__
        else:
            self.__estado__ = self.__gui__.get_variable('__estado__') if '__estado__' in self.__gui__.tkvariables else None

        self.__marco__ = self.__gui__.get_object(self.objeto_marco)

        self.__bucle__ = bucle(master=self.__marco__, con=self.con)
        self.__bucle__.funciones_activar.append(self.conectar_gui)
        self.conectar_gui()

    @_seguro
    def conectar_gui(self):
        self.__bucle__.__gui__.tkvariables[self.objeto_pos] = self.__pos__
        self.__bucle__.__gui__.tkvariables[self.objeto_tot] = self.__tot__
        self.__bucle__.__pos__ = self.__pos__
        self.__bucle__.__tot__ = self.__tot__
        self.__gui__.connect_callbacks(self.__bucle__)

    def buscar(self):pass
    def limpiar(self): pass
    def registro_ant(self):pass
    def registro_sig(self):pass


class Plan_Terapeutico(Ventana):
    variables_intercambio = ('idConsultas','fecha')

    def __init__(self, master, con):
        self.master = master
        self.con = con
        Ventana.__init__(self, master,'Plan_Terapeutico')
        gui = self.__gui__

        self.__estado__ = self.__gui__.create_variable('__estado__')

        self.__gui__.get_variable('fecha').set(time.strftime(formato_fecha_usuario))

        f_consultas=self.__gui__.get_object('Frame_Consultas')
        f_medicacion = self.__gui__.get_object('Tab_Medicacion')
        f_terapias=self.__gui__.get_object('Tab_Terapias')

        self.Control = Bucle_Consultas(f_consultas, self.con)
        self.Control.bucle.funciones_cambio_registro += (self.selec_consulta,)
        medicacion = formularios.Plan_Medicacion(f_medicacion, self.con, self.__estado__)
        medicacion.estado_nuevo()
        terapias = formularios.Plan_Tratamientos(f_terapias, self.con, self.__estado__)
        terapias.estado_nuevo()

        self.mapa = {
            str(f_medicacion) : medicacion,
            str(f_terapias) : terapias,
            }

        self.selec_tab()

    def reiniciar(self):
        pass
    def guardar(self):
        pass

    @_seguro
    def selec_consulta(self, event=None):
        sincronisar_varibles(self.Control.bucle, self, ('idConsultas','idPersonas'))
        self.selec_tab()

    @_seguro
    def selec_tab(self, event=None):
        selecion = self.__gui__.get_object('Libreta').select()
        objeto = self.mapa[selecion]

        self.__gui__.get_object('boton_reiniciar')['command']=objeto.reiniciar
        self.__gui__.get_object('boton_guardar')['command']=objeto.guardar

        sincronisar_varibles(self, objeto, self.variables_intercambio)


class Examenes(Ventana):

    variables_intercambio = ('idConsultas','fecha')

    def __init__(self, master, con):
        self.master = master
        self.con = con.copy()

        Ventana.__init__(self, master,'Pantalla_Examenes')
        self.__gui__.get_variable('fecha').set(time.strftime(formato_fecha_usuario))
        self.__estado__ = self.__gui__.create_variable('__estado__')

        f_consultas = self.__gui__.get_object('Frame_Consultas')
        f_ecg = self.__gui__.get_object('Tab_ECG')
        f_fisico = self.__gui__.get_object('Tab_Fisicos')
        f_especial = self.__gui__.get_object('Tab_Especiales')

        self.consultas = Bucle_Consultas(f_consultas, self.con)
        self.consultas.bucle.funciones_cambio_registro = self.consultas.bucle.funciones_cambio_registro + (self.selec_consulta,)
        fisico = formularios.EX_Fisicos(f_fisico, self.con, self.__estado__)
        fisico.estado_nuevo()
        ecg = formularios.EX_ECG(f_ecg, self.con, self.__estado__)
        ecg.estado_nuevo()
        especial = formularios.EX_Especiales(f_especial, self.con, self.__estado__)
        especial.estado_nuevo()

        self.mapa = {
            str(f_fisico) : fisico,
            str(f_ecg) : ecg,
            str(f_especial) : especial,
            }

        self.selec_tab()

    def reiniciar(self):
        pass
    def guardar(self):
        pass

    @_seguro
    def selec_consulta(self, event=None):
        sincronisar_varibles(self.consultas.bucle, self, ('idConsultas','idPersonas'))
        self.selec_tab()

    @_seguro
    def selec_tab(self, event=None):
        selecion = self.__gui__.get_object('Libreta').select()
        objeto = self.mapa[selecion]

        self.__gui__.get_object('boton_reiniciar')['command']=objeto.reiniciar
        self.__gui__.get_object('boton_guardar')['command']=objeto.guardar

        sincronisar_varibles(self, objeto, self.variables_intercambio)


class Bucle_Personas(Ventana):

    def __init__(self,  master, con):
        self.con = con.copy()
        self.master = master

        Ventana.__init__(self, self.master,  'C_Personas')
        self.__gui__.create_variable('int:idPersonas')

        f = (self._sincronisar,)

        marco_personas = self.__gui__.get_object('_Marco')
        self.Personas = bucles.Personas(marco_personas, self.con, sigiente=f, anterior=f, buscar=f)
        self.Personas.pos = self.__gui__.get_variable('_pos_bucle')
        self.Personas.tot = self.__gui__.get_variable('_tot_bucle')
        self.Personas.boton_sig = self.__gui__.get_object('boton_sig')
        self.Personas.boton_ant = self.__gui__.get_object('boton_ant')
        self.Personas.boton_buscar = self.__gui__.get_object('boton_buscar')

        self.__gui__.connect_callbacks(self.Personas)

    def _sincronisar(self):
        sincronisar_varibles(self.Personas, self, ('idPersonas',))

    def registro_sig(self):pass
    def registro_ant(self):pass
    def buscar(self):pass
    def reiniciar(self):pass


class Bucle_Consultas(Ventana):

    def __init__(self, master, con):
        self.master = master
        self.con = con.copy()
        Ventana.__init__(self, self.master,  'C_Consultas')
        self.__gui__.create_variable('int:idConsultas')
        self.__gui__.create_variable('int:idPersonas')
        f = (self._sincronisar,)

        marco = self.__gui__.get_object('_Marco')
        self.bucle = bucles.Consultas(marco, self.con, sigiente=f, anterior=f, buscar=f)
        self.bucle.pos = self.__gui__.get_variable('_pos_bucle')
        self.bucle.tot = self.__gui__.get_variable('_tot_bucle')
        self.bucle.boton_sig = self.__gui__.get_object('boton_sig')
        self.bucle.boton_ant = self.__gui__.get_object('boton_ant')
        self.bucle.boton_buscar = self.__gui__.get_object('boton_buscar')

        self.__gui__.connect_callbacks(self.bucle)

    def registro_sig(self):pass
    def registro_ant(self):pass
    def buscar(self):pass
    def reiniciar(self):pass

    @_seguro
    def _sincronisar(self):
        sincronisar_varibles(self.bucle, self, ('idConsultas','idPersonas'))

class Bucle_Medicos(Ventana):

    def __init__(self, master, con):
        self.master = master
        self.con = con.copy()
        Ventana.__init__(self, self.master,  'C_Medicos')
        f = (self._sincronisar,)

        marco = self.__gui__.get_object('_Marco')
        self.bucle = bucles.Medicos(marco, self.con, sigiente=f, anterior=f, buscar=f)
        self.bucle.pos = self.__gui__.get_variable('_pos_bucle')
        self.bucle.tot = self.__gui__.get_variable('_tot_bucle')
        self.bucle.boton_sig = self.__gui__.get_object('boton_sig')
        self.bucle.boton_ant = self.__gui__.get_object('boton_ant')
        self.bucle.boton_buscar = self.__gui__.get_object('boton_buscar')

        self.__gui__.tkvariables['idMedicos'] = self.bucle.__gui__.get_variable('idMedicos')
        self.idMedicos = self.__gui__.get_variable('idMedicos')

        self.__gui__.connect_callbacks(self.bucle)

    @_seguro
    def _sincronisar(self):
        sincronisar_varibles(self.bucle, self, ('idMedicos',))

    def registro_sig(self):pass
    def registro_ant(self):pass
    def buscar(self):pass
    def reiniciar(self):pass


class Bucle_Control(Ventana):
    archivo_gui = "Control_Bucle"

    def __init__(self, master, funcion):
        self.master = master
        self.funcion = funcion

        Ventana.__init__(self, self.master, self.archivo_gui)
        self.__pos__ = self.__gui__.tkvariables['pos']
        self.__tot__ = self.__gui__.tkvariables['tot']

        self.registro_actual = self.__gui__.get_object('registro_actual')
        self.registro_total = self.__gui__.get_object('registro_total')

        self.boton_sig = self.__gui__.get_object('boton_sigiente')
        self.boton_ant = self.__gui__.get_object('boton_anterior')
        self.boton_buscar = self.__gui__.get_object('boton_buscar')
        self.boton_selecionar = self.__gui__.get_object('boton_selecionar')

        self.boton_selecionar['command'] = self.funcion

    @_seguro
    def conectar_formulario(self, formulario):
        self.__gui__.connect_callbacks(formulario)
        self.boton_selecionar['command'] = self.funcion
        self.__tot__ = formulario.tot
        self.__pos__ = formulario.pos
        self._conectar_variables()
        self._conectar_botones(formulario)

    @_seguro
    def _conectar_variables(self):
        self.registro_actual['textvariable'] = self.__pos__
        self.registro_total['textvariable'] = self.__tot__

    @_seguro
    def _conectar_botones(self, formulario):
        self.boton_buscar['state'] = 'normal'
        self.boton_sig['state'] = 'normal'
        self.boton_ant['state'] = 'normal'
        formulario.boton_sig = self.boton_sig
        formulario.boton_ant = self.boton_ant
        formulario.boton_buscar = self.boton_buscar

    def aceptar(self): pass
    def selecionar(self): pass
    def registro_ant(self): pass
    def registro_sig(self): pass
    def buscar(self): pass
    def reiniciar(self): pass


class Pantalla_Crear_Consulta(Ventana, Formulario):
    variables_intercambio = ('idPersonas','idMedicos')

    def __init__(self, master, con):
        self.master = master
        self.con = con.copy()

        self.fecha = time.localtime()
        Ventana.__init__(self, master,'Pantalla_Crear_Consultas')
        self.__estado__ = self.__gui__.create_variable('__estado__')

        f_personas = self.__gui__.get_object('Frame_personas')
        self.personas = Bucle_Personas(f_personas, self.con)

        f_personas = self.__gui__.get_object('Frame_medicos')
        self.medicos = Bucle_Medicos(f_personas, self.con)

        f_consultas = self.__gui__.get_object('Frame_Consultas')
        self.consultas = formularios.Consultas(f_consultas, self.con, self.__estado__)
        self.consultas.estado_nuevo()
        self.consultas.__gui__.get_variable('fecha').set(fecha.now().strftime(formato_fecha))
        self.__gui__.get_object('boton_reiniciar')['command'] = self.reiniciar_objeto
        self.__gui__.get_object('boton_guardar')['command'] = self.consultas.guardar
        self.personas.Personas.funciones_cambio_registro += (self.selec_personas, )

    @_seguro
    def reiniciar_objeto(self):
        self.consultas.reiniciar()
        self.consultas.__gui__.get_variable('fecha').set(fecha.now().strftime(formato_fecha))

    @_seguro
    def selec_personas(self, event=None):
        sincronisar_varibles(self.personas.Personas, self.consultas,('idPersonas',))
        sincronisar_varibles(self.medicos.bucle, self.consultas,('idMedicos',))


