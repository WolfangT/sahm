#!/bin/sh
# -*- coding: utf-8 -*-

carpetaEjecucion=`pwd`
capetaArchivo=`dirname $0`

if [ "$capetaArchivo" != "." ]
then
    capetaArchivo=`dirname $0`

    if [ "$carpetaEjecucion" != "$capetaArchivo" ]
    then
        directorio="$carpetaEjecucion/$capetaArchivo"
    else
        directorio=$carpetaEjecucion
    fi
else
    directorio=$carpetaEjecucion
fi

cd $directorio

echo "Buscar actualisaciones"
# fetch changes, git stores them in FETCH_HEAD
git fetch

# check for remote changes in origin repository
newUpdatesAvailable=`git diff HEAD FETCH_HEAD`
if [ "$newUpdatesAvailable" != "" ]
then
        echo "Se encontraron actualisaciones"
        git checkout master
        git reset --hard
        git clean -f
        git pull origin master
        echo "Cambios instalados"
else
        echo "No hay actualizaciones disponibles"
fi

echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""

echo "Si el programa esta abierto se cerrara ahora, inicielo otra ves para aplicar actualizaciones"
read r

