#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  main.py
#
#  Copyright 2014 wolfang Torres <wolfang.torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import time, datetime, os, sys, traceback, subprocess
from tkinter import Tk, Toplevel, messagebox, ttk
from mysql import connector

__NOMBRE__ = 'SAHM'
__VERSION__ = 1.4
__AUTOR__ = 'Wolfang A. Torres G.'
__TITULO__ = 'Capibara Minimalista'
__FECHA__ = datetime.date(2015, 5, 14)
__CONTACTO__ = 'Wolfang.Torres@gmail.com'

import logging
logging.basicConfig(filename='registro.log', filemode='w', level=logging.DEBUG)
__registro__ = logging.getLogger(__NOMBRE__)

carpeta_madre = os.path.abspath(os.path.dirname(__file__))
carpeta_recursos = os.path.join(carpeta_madre, 'Recursos')
carpeta_guis = os.path.join(carpeta_madre, 'GUIs')
carpeta_bd = os.path.join(carpeta_madre, 'Base de Datos')
carpeta_reportes = os.path.join(carpeta_madre, 'Reportes')
iconos = os.path.join(carpeta_recursos, 'iconos_gris')
simbolos = os.path.join(carpeta_recursos, 'simbolos_orig')

_estilos = {
    'linux':'clam',
    'win32':'vista',
    }

from Libs.libsa import Ventana, Base_Entrada, Formulario, Bucle, Tabla, Reporte, _seguro, _ejecutar
from Libs.BD_XML import Tabla
from Libs.Widgets.Calendario import Calendar as calendario

from Libs.libsa import formato_fecha, formato_fecha_iso

import formularios, contenedores, bucles, mostradores, tablas

Ventana.carpeta_guis = carpeta_guis
for carpeta in (carpeta_recursos, iconos, simbolos):
    Ventana.carpetas_recursos.append(carpeta)

#from Libs.libsa import Ventana, Formulario, Bucle, Reporte, Base_Entrada, _ejecutar
#from Libs.libsa import formato_fecha_usuario, formato_fecha_iso, formato_fecha, formato_fecha_hora
#from Libs.libsa import sincronisar_varibles, timedelta_a_iso, datos_xml
#from Libs.libsa import Error, ErrorEscritura, ErrorValor, ErrorFormato
#from Libs.libsa import carpeta_madre, carpeta_bd

traducion = {'GRANT':'', 'SELECT':'Selecionar', 'INSERT':'Agregar', 'DELETE':'Borrar', ', EXECUTE':'', ', SHOW VIEW':'', 'ON':'en', 'UPDATE':'Modificar', '*.*':'Todo', '*':'Todo', 'USAGE':'Uso', '`':''}

class inicio_seccion(Ventana, Base_Entrada):
    archivo_gui = "Inicio_seccion"
    dir = os.path.join(carpeta_bd, 'BD_login.xml')

    ssl_aut = {
        'ssl_ca' : os.path.join(carpeta_bd, 'ca-cert.pem'),
        'ssl_cert' : os.path.join(carpeta_bd, 'client-cert.pem'),
        'ssl_key' : os.path.join(carpeta_bd, 'client-key.pem'),
    }

    @_seguro
    def __init__(self, master):
        self.master = master
        Ventana.__init__(self, self.master, self.archivo_gui, True)
        self.__gui__.get_object('contraseña').focus()

        self.tabla = Tabla()

        try:
            self.tabla.importar_xml(self.dir, tipo="archivo")
        except Exception as err:
            self.tabla.agregar_columna(campo='host', tipo='str', defecto='127.0.0.1')
            self.tabla.agregar_columna(campo='port', tipo='str', defecto='3306')
            self.tabla.agregar_columna(campo='user', tipo='str', defecto='')
            self.tabla.agregar_columna(campo='password', tipo='str', defecto='')
            self.tabla.agregar_columna(campo='file', tipo='str', defecto='')
            self.tabla += ()
            self.tabla.exportar_xml()
            self.tabla.escribir_xml(self.dir)

        self.sincronisar('formulario', 0, self.tabla, 'none_a_str')


    @_seguro
    def aceptar_ser(self, event=None):
        try:
            self.config = {}
            for i in ('user', 'password', 'host', 'port'):
                self.config[i] = self.datos[i]
            #~ self.config.update(self.ssl_aut)
            c = connector.connect(**self.config)
        except Exception as err:
            __registro__.debug('Intento fallido, datos {}, error {}'.format(self.config, err))
            messagebox.showinfo(message='Error de Conexión',
                                icon="error", title="Error",
                                detail="""Los datos suministrados no son validos o
No se pudo conectarse con el Servidor""")
        else:
            self.master.destroy()

    @_seguro
    def aceptar_arc(self):
        try:
            self.config = self.datos['file']
            c = connector.connect(**self.config)
        except Exception as err:
            messagebox.showinfo(message='Error de Conección',
                                icon="error", title="Error",
                                detail="""Los datos suministrados no son validos""")

    @_seguro
    def guardar(self):
        self.tabla[0] = self.datos
        self.tabla.exportar_xml()
        self.tabla.escribir_xml(self.dir)
        messagebox.showinfo(message='Guardado exitosamente',
                                icon="info", title="guardado exitoso")

    @_seguro
    def actualizar(self):
        proc = os.path.join(carpeta_madre, 'actualisar.sh')
        subprocess.call(proc, shell=True)
        sys.exit()


class main(Ventana):
    con = {}
    formato_estilo = "El estilo fue cambiado a <{}>"
    formato_estilo_error = "El estilo <{}> no esta disponible en la plataforma"

    lista_consultas = []

    def __init__(self, master):
        self.master = master
        master.wm_title(__NOMBRE__)
        Ventana.__init__(self, master, 'Main', 'menu')
        self.__estado__ = self.__gui__.get_variable('__estado__')
        self.__gui__.connect_callbacks(self)

        self._estilo()
        self._login()
        self._usuario()
        self._derechos()

        self.consultas = Tabla()
        con = self.con.copy()
        con['db'] = 'General'
        self.consultas.coneccion = con

    def proximas_consultas(self):
        if not bool(self.idMedicos): return None
        self.consultas.seleccionar_bd('Consultas_no_Asistidas_Proximas', '*', {'idMedicos':self.idMedicos})
        tabla = self.__gui__.get_object('proximas_consultas')
        for i in self.lista_consultas:
            tabla.delete(i)
        self.lista_consultas = []
        for reg in self.consultas:
            a = (time.strftime(formato_fecha_usuario, reg['fecha']), reg['pacientes'], reg['idConsultas'])
            self.lista_consultas.append(tabla.insert('', 'end', values=a))

    def _login(self):
        self.master.withdraw()
        ventana = Toplevel(self.master)
        self.login = inicio_seccion(ventana)
        self.master.wait_window(ventana)

        try :
            self.con = c = self.login.config

            self.__gui__.get_variable('derechos').set('No disponible')
            self.__gui__.get_variable('tipo').set('MySql')
            self.__gui__.get_variable('ubicacion').set(c['host'])
        except Exception:
            sys.exit()
        else:
            self.master.deiconify()

    def _estilo(self):
        self.estilo = estilo = ttk.Style()

        estilo.theme_use('clam')
        estilo.configure('TLabelframe', borderwidth=5)
        estilo.configure('.', font=('',13), background='white')
        estilo.configure('BarraEstado.TFrame', font=('',50), foreground='green')


    def _derechos(self):
        cnx = connector.connect(**self.con)
        cur = cnx.cursor()
        cur.execute('SHOW GRANTS')

        l=[]
        for reg in cur:
            t = reg[0]

            if 'TO' in t:
                t = t[:t.index('TO')]
            if 'IDENTIFIED BY' in t:
                t = t[:t.index('IDENTIFIED BY')]

            for i in traducion:
                t = t.replace(i, traducion[i])

            l.append(t)

        d = '\n'.join(l)
        self.__gui__.get_variable('derechos').set(d)

    def _usuario(self):
        cnx = connector.connect(**self.con)
        cur = cnx.cursor()
        cur.execute('SELECT USER()')
        con = cur.fetchone()

        usuario = con[0]
        user, host = usuario.split('@')

        cur.execute("SELECT idMedicos, nombre FROM Hospitales.Especialistas WHERE Usuario = '{}';".format(user))
        con = cur.fetchone()
        if bool(con):
            self.idMedicos = con[0]
            self.__gui__.get_variable('idMedicos').set(str(self.idMedicos))
            self.__gui__.get_variable('usuario').set(con[1])
        else:
            self.idMedicos = None
            self.__gui__.get_variable('idMedicos').set('-')
            self.__gui__.get_variable('usuario').set(user)
        __registro__.debug('Iniciando como Medico {}; datos: {}'.format(self.idMedicos, con))

    def acerca(self):
        v = Toplevel(self.master)
        v.wm_title("{} - Verción {}".format(__NOMBRE__, __VERSION__))
        a = acerca(v)

    def mod_consulta(self):
        consultas = Toplevel(self.master)
        consultas.wm_title("{} - Control de Consultas".format(__NOMBRE__))
        c = Pantalla_Consultas(consultas, self.con)

    def crear_consulta(self):
        consultas = Toplevel(self.master)
        consultas.wm_title("{} - Crear Consultas".format(__NOMBRE__))
        c = contenedores.Pantalla_Crear_Consulta(consultas, self.con)
        c.medicos.__gui__.get_variable('idMedicos').set(self.idMedicos)

    def paciente_nuevo(self):
        ven = Toplevel(self.master)
        ven.geometry('{}x{}+0+0'.format(ANCHO//2,ALTO))
        ven.wm_title("{} - Pacientes".format(__NOMBRE__))
        p = Paciente_Nuevo(ven, self.con, self.idMedicos)

    def pacientes(self):
        ven = Toplevel(self.master)
        ven.wm_title("{} - Pacientes".format(__NOMBRE__))
        p = Selecion_Consulta(ven, self.con, self.idMedicos)

    def historia_clinica(self):
        ven = Toplevel(self.master)
        ven.wm_title("{} - Historia Clinica".format(__NOMBRE__))
        ven.geometry('{}x{}+0+0'.format(ANCHO,ALTO))
        h = Historia_Medica(ven, self.con)

    def medicos(self):
        ven = Toplevel(self.master)
        ven.wm_title("{} - Medicos".format(__NOMBRE__))
        form = formularios.Especialistas
        medico = contenedores.Contenedor_Formulario(ven, self.con, form)
        medico.formulario.estado_nuevo()

    def centros_medicos(self):
        ven = Toplevel(self.master)
        ven.wm_title("{} - Centros Medicos".format(__NOMBRE__))
        form = formularios.CentrosMedicos
        contenedor = contenedores.Contenedor_Formulario(ven, self.con, form)
        contenedor.formulario.estado_nuevo()

    def _estilo_default(self):
        try:
            self.estilo.theme_use('default')
            self.__estado__.set(self.formato_estilo.format('Defecto'))
        except Exception:
            self.__estado__.set(self.formato_estilo_error.format('default'))

    def _estilo_clam(self):
        try:
            self.estilo.theme_use('clam')
            self.__estado__.set(self.formato_estilo.format('Clam'))
        except Exception:
            self.__estado__.set(self.formato_estilo_error.format('Clam'))

    def _estilo_vista(self):
        try:
            self.estilo.theme_use('vista')
            self.__estado__.set(self.formato_estilo.format('Vista'))
        except Exception:
            self.__estado__.set(self.formato_estilo_error.format('Vista'))


class Paciente_Nuevo(Ventana, Formulario):

    archivo_gui = "C_Paciente_Nuevo"

    consultas = Tabla()
    nombre_tabla_c = 'Consultas'
    cols_c = ('idPersonas', 'idMedicos', 'idHospitales', 'motivo', 'asistida', 'notas')
    clausula_c = False

    def __init__(self, master, con, idMedicos):
        self.idMedicos = idMedicos
        self.master = master
        self.con = con.copy()
        self.cong = con.copy()
        self.conp = con.copy()
        self.conm = con.copy()
        self.cong['db'] = 'General'
        self.conp['db'] = 'Pacientes'

        Ventana.__init__(self, self.master, self.archivo_gui)
        self.__estado__ = self.__gui__.get_variable('__estado__')

        Formulario.__init__(self, self.consultas)
        self.f_persona()
        self.f_carac()
        self.f_cont()
        self.f_antecedentes()
        self.extra()

    def extra(self):
        self.consultas.coneccion = (self.cong)
        self.consultas.seleccionar_bd(self.nombre_tabla_c, self.cols_c, self.clausula_c)

    def f_persona(self):
        marco_persona = self.__gui__.get_object('persona')
        self.persona = formularios.Personas(marco_persona, self.con, self.__estado__)
        self.persona.estado_nuevo()

    def f_antecedentes(self):
        marco_ant = self.__gui__.get_object('antecedente_rap')
        self.f_ant = formularios.Ant_rap(marco_ant, self.conp, self.__estado__)
        self.f_ant.estado_nuevo()

    def f_carac(self):
        marco_carac = self.__gui__.get_object('caracteristica')
        self.caracteristica = formularios.Caracteristicas(marco_carac, self.conp, 0, self.__estado__)
        self.caracteristica.estado_nuevo()

    def f_cont(self):
        marco_cont = self.__gui__.get_object('contacto')
        self.contacto = formularios.Contacto(marco_cont, self.con, self.__estado__)
        self.contacto.estado_nuevo()

    @_seguro
    def cancelar(self):
        self.master.destroy()

    @_seguro
    def guardar(self):
        r = _ejecutar([
            self.persona._validar_todo,
            self.caracteristica._validar_todo,
            self.contacto._validar_todo,
            self.f_ant._validar_todo
           ])
        if not r: return False

        cnx = connector.connect(**self.con)
        cnx.start_transaction()
        cur = cnx.cursor()

        try:
            cur.execute('USE Pacientes')

            self.persona.cnx = cnx
            self.persona.guardar()
            self.idPersonas = self.persona.id

            cur.execute('USE General')
            self.consultas += {
            'idPersonas' : self.idPersonas,
            'idMedicos' : self.idMedicos,
            'idHospitales' : 1,
            'motivo' : 'Obtención inicial de datos',
            'asistida' : 1,
            'notas' : '',
            }
            self.idConsultas = self.consultas.insertar_bd(cnx=cnx)

            cur.execute('USE Pacientes')

            self.caracteristica.__gui__.get_variable('idPersonas').set(self.idPersonas)
            self.caracteristica.cnx = cnx
            self.caracteristica.guardar()

            self.contacto.__gui__.get_variable('idPersonas').set(self.idPersonas)
            self.contacto.cnx = cnx
            self.contacto.guardar()

            self.f_ant.__gui__.get_variable('idConsultas').set(self.idConsultas)
            self.f_ant.cnx = cnx
            self.f_ant.guardar()

            medicos = Tabla('Personas_Especialistas', self.conp.copy())
            medicos.seleccionar_bd(cols=('idPersonas','idMedicos'), condicion=False)
            medicos += {
            'idPersonas':self.idPersonas,
            'idMedicos':self.idMedicos,
            }
            medicos.insertar_bd(cnx=cnx)

        except Exception as err:
            cnx.rollback()
            raise err

        else:
            cnx.commit()
            self.iniciar_consulta()

        finally:
            cur.close()
            cnx.close()
            self.master.withdraw()

    @_seguro
    def iniciar_consulta(self):
        motivo = 'Primera consulta'

        consulta = Tabla('Consultas', self.cong)
        consulta.seleccionar_bd(cols=('idMedicos', 'idPersonas', 'idHospitales', 'motivo', 'asistida'), condicion=False)
        consulta += {'idMedicos':self.idMedicos, 'idPersonas':self.idPersonas, 'idHospitales':1, 'motivo':motivo, 'asistida':1}

        try:
            self.idConsultas = consulta.insertar_bd()
        except bd.Error as err:
            msj = '''No se pudo Crear un Consulta, debido a:
{}'''.format(err)
            Error(msj, self).mensaje()
        else:
            ventana = Toplevel(self.master)
            ventana.wm_title("{} - Consulta Inicial".format(__NOMBRE__))
            ventana.geometry('{}x{}+0+0'.format(ANCHO,ALTO))
            Pantalla_Consulta_Foco_Pacientes(ventana, self.con, self.idPersonas, self.idConsultas, self.idMedicos)


class PacienteExistente(Ventana, Formulario):

    archivo_gui = "C_Paciente_Existente"

    def __init__(self, master, con, idPersonas):
        self.idPersonas = idPersonas
        self.master = master
        self.con = con.copy()
        Ventana.__init__(self, self.master, self.archivo_gui)
        self.__estado__ = self.__gui__.get_variable('__estado__')
        self.formularioPersona()
        self.formularioCaracteristicas()
        self.formularioContacto()

    def formularioPersona(self):
        marco_persona = self.__gui__.get_object('persona')
        self.persona = formularios.PersonasExistente(marco_persona, self.con, self.__estado__)
        self.persona.estado_nuevo()
        self.persona.tabla.seleccionar_bd(
            tabla=self.persona.nombre_tabla,
            cols=('*',),
            condicion={'idPersonas':self.idPersonas})
        self.persona.sincronisar(
            'formulario',
            0,
            self.persona.tabla,
            'no_pri','none_a_str')
        self.persona._escribir()

    def formularioCaracteristicas(self):
        marco_carac = self.__gui__.get_object('caracteristica')
        self.caracteristica = formularios.Caracteristicas(marco_carac, self.con, 0, self.__estado__)
        self.caracteristica.estado_nuevo()
        self.caracteristica.tabla.seleccionar_bd(
            tabla=self.caracteristica.nombre_tabla,
            cols=('*','ultima_fecha_actualisacion', 'idCaracteristicas'),
            condicion={'idPersonas':self.idPersonas})
        self.caracteristica.sincronisar(
            'formulario',
            0,
            self.caracteristica.tabla,
            'no_pri','none_a_str')

    def formularioContacto(self):
        marco_cont = self.__gui__.get_object('contacto')
        self.contacto = formularios.Contacto(marco_cont, self.con, self.__estado__)
        self.contacto.estado_nuevo()
        self.contacto.tabla.seleccionar_bd(
            tabla=self.contacto.nombre_tabla,
            cols=('*', 'id'),
            condicion={'idPersonas':self.idPersonas})
        self.contacto.sincronisar(
            'formulario',
            0,
            self.contacto.tabla,
            'no_pri','none_a_str')

    @_seguro
    def cancelar(self):
        self.master.destroy()

    @_seguro
    def guardar(self):
        r = _ejecutar([
            self.persona._validar_todo,
            self.caracteristica._validar_todo,
            self.contacto._validar_todo,
           ])
        if not r: return False
        cnx = connector.connect(**self.con)
        cnx.start_transaction()
        cur = cnx.cursor()
        try:
            cur.execute('USE Pacientes')
            self.persona.tabla[0] = self.persona.datos
            self.persona.tabla[0]['idPersonas'] = self.idPersonas
            self.persona._guardar()
            self.persona.tabla.actualisar_bd(cnx=cnx)
            cur.execute('USE Pacientes')
            self.caracteristica.tabla[0] = self.caracteristica.datos
            self.caracteristica.tabla[0]['idPersonas'] = self.idPersonas
            self.caracteristica.tabla.actualisar_bd(cnx=cnx)
            __registro__.debug('empesar a act contacto')
            self.contacto.tabla[0] = self.contacto.datos
            self.contacto.tabla[0]['idPersonas'] = self.idPersonas
            self.contacto.tabla.actualisar_bd(cnx=cnx, llaves=['idPersonas'])
        except Exception as err:
            cnx.rollback()
            raise err
        else:
            cnx.commit()
        finally:
            cur.close()
            cnx.close()
            self.master.withdraw()


class Pantalla_Consulta_Foco_Pacientes(Ventana):

    archivo_gui = 'Pantalla_Paciente'

    def __init__(self, master, con, idPersonas, idConsultas, idMedicos):
        self.tablas = []
        self.master = master
        self.con = con.copy()
        self.idMedicos = idMedicos
        self.idPersonas = idPersonas
        self.idConsultas = idConsultas

        Ventana.__init__(self, master, self.archivo_gui, 'Menu')
        self.__estado__ = self.__gui__.get_variable('__estado__')

        self._agregar_menu()
        self._agregar_pacientes()
        self._agregar_diagnosticos()
        self._agregar_antecedentes()
        self._agregar_mediacion()
        self._agregar_tratamientos()
        self._agregar_plan_diagnostico()
        self._agregar_historia()

    def _agregar_pacientes(self):
        marco = self.__gui__.get_object('paciente')
        self.mostrador_paciente = mostradores.pacientes(marco, self.con, self.idPersonas)

    def _agregar_diagnosticos(self):
        marco = self.__gui__.get_object('diagnostico')
        self.diagnosticos = tablas.Diagnosticos(marco, self.con, self.idPersonas)
        self.tablas.append( self.diagnosticos)

    def _agregar_antecedentes(self):
        marco = self.__gui__.get_object('antecedente')
        self.antecedentes = tablas.Antecedentes(marco, self.con, self.idPersonas)
        self.tablas.append( self.antecedentes)

    def _agregar_mediacion(self):
        marco = self.__gui__.get_object('medicacion')
        self.medicacion = tablas.Medicacion(marco, self.con, self.idPersonas)
        self.tablas.append( self.medicacion)

    def _agregar_tratamientos(self):
        marco = self.__gui__.get_object('tratamientos')
        self.tratamientos = tablas.Tratamientos(marco, self.con, self.idPersonas)
        self.tablas.append( self.tratamientos)

    def _agregar_plan_diagnostico(self):
        marco = self.__gui__.get_object('planDiagnostico')
        self.plan_diagnostico = tablas.Plan_Diagnostico(marco, self.con, self.idPersonas)
        self.tablas.append( self.plan_diagnostico)

    def _agregar_historia(self):
        marco_h = self.__gui__.get_object('historia_clinica')
        marco_e = self.__gui__.get_object('evento')
        t = tablas.Histora_Clinica(marco_h, marco_e, self.con, self.idPersonas)
        self.tablas.append( t)

    def _agregar_menu(self):
        menu = self.__gui__.get_object('menu')

        if self.idConsultas is None:
            menu.insert('', 0, text='Menu Desabilitado')
            menu.insert('', 1, text='Se requiere de una consulta')
            return None

        menu.bind('<Double-1>', self.selecionar_menu)

        sintomas = menu.insert('', 'end', text='Sintomas')

        signo = menu.insert('', 'end', text='Signos Clinicos')

        antecedentes = menu.insert('', 'end', text='Antecedentes')

        examenes = menu.insert('', 'end', text='Examenes')
        ex_fisico = menu.insert(examenes, 'end', text='Ex Fisico')
        ex_ecg = menu.insert(examenes, 'end', text='ECG')
        ex_especial = menu.insert(examenes, 'end', text='Ex Especiales')

        diagnosticos = menu.insert('', 'end', text='Diagnosticos')

        plan_diagnostico = menu.insert('', 'end', text='Plan Diagnostico')

        terapias = menu.insert('', 'end', text='Plan Terapeutico')
        plan_medicacion = menu.insert(terapias, 'end', text='Tx Farmacologicos')
        plan_tratamiento = menu.insert(terapias, 'end', text='Tx No Farmacologicos')

        self._mapeo_menu_formulario = {
            diagnosticos:       formularios.Diagnosticos,
            antecedentes:       formularios.Antecedentes,
            sintomas:           formularios.Sintomas,
            ex_fisico:          formularios.EX_Fisicos,
            ex_ecg:             formularios.EX_ECG,
            signo:              formularios.Signos,
            ex_especial:        formularios.EX_Especiales,
            plan_medicacion:    formularios.Plan_Medicacion,
            plan_tratamiento:   formularios.Plan_Tratamiento,
            plan_diagnostico:   formularios.Plan_Diagnostico,
        }

    @_seguro
    def selecionar_menu(self, evento):
        menu = evento.widget
        objeto = menu.selection()[0]
        if objeto in self._mapeo_menu_formulario:
            formulario = self._mapeo_menu_formulario[objeto]

            v = Toplevel(self.master)
            v.wm_title( "{} - {}".format(__NOMBRE__,menu.item(objeto, 'text')) )
            self.f = f = contenedores.Contenedor_Formulario(v, self.con, formulario, self.__estado__)
            f.formulario.estado_nuevo()
            f.formulario.__gui__.get_variable('idConsultas').set(self.idConsultas)
            f.formulario.funciones_guardar += (self.reiniciar_tablas, f.cancelar, )
            __registro__.debug(str(f.formulario.funciones_guardar))

    def destruir_formulario(self):
        self.f.master.destroy()
        return True

    def reiniciar_tablas(self):
        for obj in self.tablas:
            obj._tabla()
        return True

    def convertir_en_diagnostico(self):
        if self.idConsultas is None:
            return None
        self.antecedentes.convertir_en_diagnostico()
        self.reiniciar_tablas()

    def convertir_en_antecedente(self):
        if self.idConsultas is None:
            return None
        self.diagnosticos.convertir_en_antecedente()
        self.reiniciar_tablas()

    def terminar_tratamiento(self):
        if self.idConsultas is None:
            return None
        self.tratamientos.terminar_tratamiento()
        self.reiniciar_tablas()

    def terminar_medicacion(self):
        if self.idConsultas is None:
            return None
        self.medicacion.teminar_medicacion()
        self.reiniciar_tablas()

    def terminar_plan_diagnostico(self):
        if self.idConsultas is None:
            return None
        self.plan_diagnostico.entregar_examen()
        self.reiniciar_tablas()

    def terminar_consulta(self):
        if self.idConsultas is None:
            return None

        v = Toplevel(self.master)
        v.wm_title( "{} - {}".format(__NOMBRE__, "Información de Consulta") )
        f = contenedores.Contenedor_Formulario(v, self.con, formularios.Inf_Consulta, self.__estado__)
        f.formulario.estado_nuevo()
        f.formulario.id = self.idConsultas

        self.master.wait_window(v)

        #Crear tablas
        consulta = Tabla()
        con = self.con.copy()
        con['db'] = 'General'
        consulta.coneccion = (con)
        consulta.seleccionar_bd('Consultas', ('*', ), {'idConsultas':self.idConsultas})

        #Guardar datos
        consulta[0]['asistida'] = 1
        consulta.actualisar_bd('Consultas', ['asistida'])

        #Avisar al usuario
        self.__estado__.set('''Consulta ID {} fue sido terminada'''.format(self.idConsultas))

    def imprimir_recipe(self):
        imprimir_reporte(('Paciente','Metadatos'), self.con, 'formato_recipe_farmacia', self.__estado__, idPersonas=self.idPersonas, idMedicos=self.idMedicos, idHospitales=1)

    def imprimir_indicaciones(self):
        imprimir_reporte(('Paciente','Metadatos'), self.con, 'formato_recipe_paciente', self.__estado__, idPersonas=self.idPersonas, idMedicos=self.idMedicos, idHospitales=1)

    def imprimir_recipe_indicaciones(self):
        imprimir_reporte(('Paciente','Metadatos'), self.con, 'formato_recipe_paciente_farmacia', self.__estado__, idPersonas=self.idPersonas, idMedicos=self.idMedicos, idHospitales=1)

    def imprimir_historia(self):
        imprimir_reporte('Historia', self.con, 'formato_historia_medica', self.__estado__, idPersonas=self.idPersonas, idMedicos=self.idMedicos)

    def imprimir_informe(self):
        if not self.idConsultas:
            return False
        imprimir_reporte(('Consulta','Paciente'), self.con, 'formato_informe_medico', self.__estado__, idPersonas=self.idPersonas, idMedicos=self.idMedicos, idConsultas=self.idConsultas, idHospitales=1)


class Selecion_Consulta(Ventana):

    archivo_gui = "C_Selecion_Consulta"
    objeto_marco_personas = '__marco__'

    consultas = Tabla()
    nombre_consultas = 'Consultas'
    columnas_consultas = ('idPersonas', 'idMedicos','motivo', 'notas', 'idHospitales')
    condicion_consultas = False

    idPersonas = 0
    idConsultas = None
    idMedicos = None

    def __init__(self, master, con, idMedicos=None):
        self.master = master
        self.idMedicos = idMedicos
        self.con = con.copy()
        self.con['db'] = 'General'

        #actualisar la tabla
        self.consultas.coneccion = (self.con)
        self.consultas.seleccionar_bd(self.nombre_consultas, self.columnas_consultas, self.condicion_consultas)
        self.consultas += {}

        #crear la interfas, colocar los bucles y conectar las variables
        Ventana.__init__(self, self.master, self.archivo_gui)

        marco_p = self.__gui__.get_object(self.objeto_marco_personas)
        self.bucle_personas = contenedores.Contenedor_Bucle_Busqueda(marco_p, self.con, bucles.Personas)

    @_seguro
    def modificar_paciente(self):
        self.idPersonas = self.bucle_personas.__bucle__.__gui__.get_variable('idPersonas').get()
        if not bool(self.idPersonas): return None
        ven = Toplevel(self.master)
        ven.wm_title("{} - Pacientes".format(__NOMBRE__))
        __registro__.debug('modificando paciente: {}<{}>'.format(self.idPersonas, type(self.idPersonas)))
        p = PacienteExistente(ven, self.con, self.idPersonas)
        self.master.withdraw()

    @_seguro
    def consulta_nueva(self):
        self.idPersonas = self.bucle_personas.__bucle__.__gui__.get_variable('idPersonas').get()
        if not bool(self.idPersonas): return None
        if not bool(self.idMedicos): return None
        self.__gui__.create_variable('int:idMedicos').set(self.idMedicos)
        self.__gui__.create_variable('int:idHospitales').set(1)
        self.__gui__.create_variable('int:idPersonas').set(self.idPersonas)
        self.consultas[0] = self.datos

        try:
            self.idConsultas = self.consultas.insertar_bd()
        except bd.Error as err:
            msj = '''No se pudo Crear un Consulta, debido a:
{}'''.format(err)
            Error(msj, self).mensaje()
        else:
            self.crear_pantalla_pacientes()

    def sin_consulta(self):
        self.idConsultas = None
        self.idPersonas = self.bucle_personas.__bucle__.__gui__.get_variable('idPersonas').get()
        if not bool(self.idPersonas): return None

        self.crear_pantalla_pacientes()

    def crear_pantalla_pacientes(self):
        ven = Toplevel(self.master)
        ven.wm_title("{} - Pacientes".format(__NOMBRE__))
        ven.geometry('{}x{}+0+0'.format(ANCHO,ALTO))
        p = Pantalla_Consulta_Foco_Pacientes(ven, self.con, self.idPersonas, self.idConsultas, self.idMedicos)
        self.master.withdraw()


@_seguro
def reporte_metadatos(con, idMedicos, idHospitales, idPersonas):
    #Nombres de las bases de datos
    con = con.copy()
    con['db'] = 'Hospitales'
    conp = con.copy()
    conp['db'] = 'Pacientes'

    #Obteniendo tablas
    hospital = Tabla('Hospitales', coneccion=con)
    medico = Tabla('Medicos', coneccion=con)
    persona = Tabla('Pacientes', coneccion=conp)
    caracteristica = Tabla('Caracteristicas', coneccion=conp)
    contacto = Tabla('Contacto', coneccion=conp)

    #Creando Coneciones
    condicion_m = {'idMedicos':idMedicos}
    condicion_h = {'idHospitales':idHospitales}
    condicion_p = {'idPersonas':idPersonas}

    #Selecionar Informacion
    hospital.seleccionar_bd(condicion=condicion_h)
    medico.seleccionar_bd(condicion=condicion_m)
    persona.seleccionar_bd(condicion=condicion_p)
    caracteristica.seleccionar_bd(condicion=condicion_p)
    contacto.seleccionar_bd(condicion=condicion_p)

    #crear reporte
    tablas = [persona, caracteristica, contacto, hospital, medico]
    #reporte = Reporte(persona,diagnosticos, medicacion, terapias,plan_diagnostico, hospital, medico, hoja_estilo=(hoja_estilo+'.xsl'))
    #xml = reporte.reporte_xml_estilo()

    return tablas

@_seguro
def reporte_paciente(con, idPersonas):
    #Nombres de las bases de datos
    cont = con.copy()
    cont['db'] = 'Tratamientos'
    conp = con.copy()
    conp['db'] = 'Pacientes'

    #Obteniendo tablas
    diagnosticos = Tabla('Diagnosticos_Activos', coneccion=conp)
    medicacion = Tabla('Medicacion_Activa', coneccion=cont)
    terapias = Tabla('Tratamientos_Activos', coneccion=cont)
    plan_diagnostico = Tabla('Examenes_Pedidos', coneccion=cont)

    #Crear Condiciones
    condicion_p = {'idPersonas':idPersonas}

    #Obtener datos
    diagnosticos.seleccionar_bd(condicion=condicion_p)
    medicacion.seleccionar_bd(condicion=condicion_p)
    terapias.seleccionar_bd(condicion=condicion_p)
    plan_diagnostico.seleccionar_bd(condicion=condicion_p)

    #crear reporte
    tablas = [diagnosticos, medicacion, terapias, plan_diagnostico]

    return tablas

@_seguro
def reporte_consulta(con, idConsultas):
    #Nombres de las bases de datos
    cons = con.copy()
    cons['db'] = 'Semiologia'
    cont = con.copy()
    cont['db'] = 'Tratamientos'
    conp = con.copy()
    conp['db'] = 'Pacientes'
    cong = con.copy()
    cong['db'] = 'General'
    conh = con.copy()
    conh['db'] = 'Hospitales'

    #Obteniendo tablas
    consulta = Tabla('Consultas', coneccion=cong)

    medicacion = Tabla('Plan_Tratamientos_Farmacologicos', coneccion=cont)
    tratamientos = Tabla('Plan_Tratamientos_No_Farmacologicos', coneccion=cont)

    sintomas = Tabla('Sintomas', coneccion=cons)
    signos = Tabla('Signos', coneccion=cons)

    persona = Tabla('Pacientes', coneccion=conp)
    diagnosticos = Tabla('Diagnosticos', coneccion=conp)

    hospital = Tabla('Hospitales', coneccion=conh)
    medico = Tabla('Medicos', coneccion=conh)

    #Obtener el resto de la información del registro Consulta
    condicion_c = {'idConsultas':idConsultas}
    consulta.seleccionar_bd(condicion=condicion_c)

    idPersonas = consulta[0]['idPersonas']
    idMedicos = consulta[0]['idMedicos']
    idHospitales = consulta[0]['idHospitales']
    condicion_p = {'idPersonas':idPersonas}
    condicion_m = {'idMedicos':idMedicos}
    condicion_h = {'idHospitales':idHospitales}

    #Importar el resto de las tablas
    persona.seleccionar_bd(condicion=condicion_p)
    medicacion.seleccionar_bd(condicion=condicion_c)
    tratamientos.seleccionar_bd(condicion=condicion_c)
    sintomas.seleccionar_bd(condicion=condicion_c)
    signos.seleccionar_bd(condicion=condicion_c)
    diagnosticos.seleccionar_bd(condicion=condicion_c)
    hospital.seleccionar_bd(condicion=condicion_h)
    medico.seleccionar_bd(condicion=condicion_m)

    __registro__.debug("""Informe Medico:
    Registro Consulta: {}
    idPersonas = {}
    idMedicos = {}
    idHospitales = {}""".format(consulta[0], persona[0], medico[0], hospital[0]))

    #crear reporte
    tablas = [sintomas, signos, diagnosticos, medicacion, tratamientos, hospital, medico, persona, consulta]

    return tablas

@_seguro
def reporte_historia(con, idPersonas, idHospitales, idMedicos):
    #nombres de las bases de datos
    cons = con.copy()
    cons['db'] = 'Semiologia'
    cont = con.copy()
    cont['db'] = 'Tratamientos'
    conp = con.copy()
    conp['db'] = 'Pacientes'
    cong = con.copy()
    cong['db'] = 'General'
    conh = con.copy()
    conh['db'] = 'Hospitales'

    #Obteniendo tablas
    hospital = Tabla('Hospitales', coneccion=conh)
    medico = Tabla('Medicos', coneccion=conh)

    persona = Tabla('Personas', coneccion=conp)
    caracteristica = Tabla('Caracteristicas', coneccion=conp)
    contacto = Tabla('Contacto', coneccion=conp)

    diagnostico = Tabla('Diagnosticos', coneccion=conp)

    consulta = Tabla('Consultas', coneccion=cong)
    historial = Tabla('Historial', coneccion=cong)

    medicacion = Tabla('Plan_Tratamientos_Farmacologicos', coneccion=cont)
    tratamiento = Tabla('Plan_Tratamientos_No_Farmacologicos', coneccion=cont)
    plan_diagnostico = Tabla('Plan_Dianostico', coneccion=cont)

    sintoma = Tabla('Sintomas', coneccion=cons)
    signo = Tabla('Signos', coneccion=cons)
    ex_fisico = Tabla('Ex_Especiales', coneccion=cons)
    ex_ecg = Tabla('Ex_Especiales', coneccion=cons)
    ex_especial = Tabla('Ex_Especiales', coneccion=cons)

    #Obtener el resto de la información del registro Consulta
    condicion_h = {'idHospitales':idHospitales}
    hospital.seleccionar_bd(condicion=condicion_h)

    condicion_m = {'idMedicos':idMedicos}
    medico.seleccionar_bd(condicion=condicion_m)

    condicion_p = {'idPersonas':idPersonas}
    persona.seleccionar_bd(condicion=condicion_p)
    caracteristica.seleccionar_bd(condicion=condicion_p)
    contacto.seleccionar_bd(condicion=condicion_p)

    historial.seleccionar_bd(condicion=condicion_p)
    consulta.seleccionar_bd(condicion=condicion_p)

    #Importar el resto de las tablas
    #diagnosticos.seleccionar_bd(condicion=condicion_c)
    #medicacion.seleccionar_bd(condicion=condicion_c)
    #tratamientos.seleccionar_bd(condicion=condicion_c)
    #plan_diagnostico.seleccionar_bd(condicion=condicion_c)
    #sintomas.seleccionar_bd(condicion=condicion_c)
    #signos.seleccionar_bd(condicion=condicion_c)
    #ex_fisico.seleccionar_bd(condicion=condicion_c)
    #ex_ecg.seleccionar_bd(condicion=condicion_c)
    #ex_especial.seleccionar_bd(condicion=condicion_c)

    #crear reporte
    tablas = [medico, hospital, persona, caracteristica, contacto, diagnostico, consulta, historial, medicacion, tratamiento, plan_diagnostico, sintoma, signo, ex_fisico, ex_ecg, ex_especial]

    #reporte = Reporte(*tablas, hoja_estilo=(hoja_estilo+'.xsl'))
    #xml = reporte.reporte_xml_estilo()

    return tablas

@_seguro
def imprimir_reporte(tipo, con, hoja_estilo, __estado__=None, **kwarg):
    idConsultas = kwarg.get('idConsultas')
    idPersonas = kwarg.get('idPersonas')
    idMedicos = kwarg.get('idMedicos')
    idHospitales = kwarg.get('idHospitales')

    tablas = []

    if "Metadatos" in tipo:
        for i in (idPersonas, idMedicos, idHospitales):
            if not i: raise AttributeError('Reporte tipo <Metadatos> requiere <idPersonas>, <idMedicos>, <idHospitales>')
        nombre = '''P{}_F{}.xml'''.format(idPersonas, time.strftime(formato_fecha))
        tablas += reporte_metadatos(con, idMedicos, idHospitales, idPersonas)

    if "Paciente" in tipo:
        if not idPersonas: raise AttributeError('Reporte tipo <Paciente> requiere <idPersonas>')
        nombre = '''P{}_F{}.xml'''.format(idPersonas, time.strftime(formato_fecha))
        tablas += reporte_paciente(con, idPersonas)

    if "Consulta" in tipo:
        if not idConsultas: raise AttributeError('Reporte tipo <Consulta> requiere <idConsultas>')
        nombre = '''C{}_F{}.xml'''.format(idConsultas, time.strftime(formato_fecha))
        tablas += reporte_consulta(con, idConsultas)

    if "Historia" in tipo:
        for i in (idPersonas, idMedicos, idHospitales):
            if not i: raise AttributeError('Reporte tipo <Historia> requiere <{}>'.format(i))
        nombre = '''H_P{}_F{}.xml'''.format(idPersonas, time.strftime(formato_fecha))
        tablas += reporte_historia(con, idPersonas, idHospitales, idMedicos)

    reporte = Reporte(*tablas, hoja_estilo=(hoja_estilo+'.xsl'))
    xml = reporte.reporte_xml_estilo()

    carpeta = carpeta_reportes
    dir = os.path.join(carpeta, nombre)

    with open(dir, 'w') as archivo: archivo.write(xml)

    #Abrir el reporte
    if sys.platform.startswith('linux'):
        subprocess.Popen(['xdg-open', dir])
    if sys.platform.startswith('win'):
        os.startfile(dir)

    if not __estado__ is None:
        __estado__.set('''Reporte "{nombre}" Generado en la carpeta "{carpeta}"'''.format(**locals()))


class acerca(Ventana):
    def __init__(self, master):
        self.master = master
        master.wm_title(__NOMBRE__)
        Ventana.__init__(self, master, 'acerca')
        self.valores()

    def valores(self):
        self.__gui__.get_variable('vercion').set('{} - Ver. {}'.format(__NOMBRE__,__VERSION__))
        self.__gui__.get_variable('titulo').set(__TITULO__)
        self.__gui__.get_variable('fecha').set(__FECHA__.strftime('%d/%m/%Y'))
        self.__gui__.get_variable('autor').set(__AUTOR__)
        self.__gui__.get_variable('contacto').set(__CONTACTO__)

    def licencia(self):
        archivo = os.path.abspath(os.path.join(os.path.dirname(__file__),'Licencia.html'))
        subprocess.Popen([archivo])

    def actualisar(self):
        proc = os.path.join(carpeta_madre, 'actualisar.sh')
        subprocess.call(proc, shell=True)
        sys.exit()

    def manual(self):
        proc = os.path.join(carpeta_madre, 'manual.pdf')
        if sys.platform.startswith('linux'):
            subprocess.Popen(['xdg-open', proc])
        if sys.platform.startswith('win'):
            os.startfile(proc)

if __name__ == '__main__':
    try:
        root = Tk()
        ANCHO, ALTO = root.winfo_screenwidth(), root.winfo_screenheight()
        SAHM = main(root)
        root.mainloop()

    except Exception:
        reg = os.path.join(carpeta_madre, 'registro.log')
        error = traceback.format_exc()
        __registro__.critical(error)
        msj = '''Ha ocurrido un error totalmente inesperado y el programa no va a poder continuar,
Por favor envie el archivo
{reg}
 a {autor} en {correo}
        '''.format(reg=reg, autor=__AUTOR__, correo=__CONTACTO__)
        messagebox.showerror(title='Error Critico Encontrado', icon="error", message=msj)
