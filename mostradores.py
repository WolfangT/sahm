#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  main.py
#
#  Copyright 2014 wolfang Torres <wolfang.torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import time, datetime
from datetime import datetime as fecha
from tkinter import ttk, messagebox, Tk, Toplevel
from tkinter.ttk import Label as label, LabelFrame as lf

from Libs.libsa import Ventana, Formulario, Bucle, Reporte, Base_Entrada
from Libs.libsa import formato_fecha_usuario, formato_fecha_iso, formato_fecha, formato_fecha_hora
from Libs.libsa import sincronisar_varibles, timedelta_a_iso, datos_xml
from Libs.libsa import Error, ErrorEscritura, ErrorValor, ErrorFormato, _seguro

from Libs.BD_XML import Tabla
from Libs.Widgets.Calendario import Calendar as calendario

class Mostrador_Estandar(Ventana, Base_Entrada):

    def __init__(self, master, con, condicion=False):

        self.master = master
        self.con = con.copy()
        self.con['db'] = self.bd
        self.condicion = condicion

        self.tabla = Tabla()

        self._gui()
        self._tabla()

        self.sincronisar('formulario', 0, self.tabla, 'none_a_str')

    def _gui(self):
        Ventana.__init__(self, self.master, self.archivo_gui)

    def _tabla(self):
        self.tabla.coneccion = (self.con)
        self.tabla.seleccionar_bd(self.nombre_tabla, self.columnas, self.condicion)

class pacientes(Mostrador_Estandar):

    archivo_gui = 'M_Pacientes'
    condicion = {'idPersonas':None}


    def __init__(self, master, con, id):
        self.condicion = {'idPersonas':id}

        self.nombre_tabla = 'Pacientes'
        self.columnas = ('*','fecha_nacimiento', 'telefono', 'direccion', 'correo_electronico')
        self.bd = 'Pacientes'

        #self.tabla_d = 'Antecedentes'
        #self.columnas_d = ('idPersonas','nombre',)
        #self.bd_d = 'Pacientes'

        self.tabla_c = 'Caracteristicas'
        self.columnas_c = ('*', 'idCaracteristicas')
        self.bd_c = 'Pacientes'

        self.tabla_i = 'Contacto'
        self.columnas_i = ('*', 'id')
        self.bd_i = 'Pacientes'

        Mostrador_Estandar.__init__(self, master, con, self.condicion)
        marco_a = self.__gui__.get_object('__antecedentes__')

        #self.diagnosticos = Tabla()
        #cond = con.copy()
        #cond['db'] = self.bd_d
        #self.diagnosticos.coneccion = (cond)
        #self.diagnosticos.seleccionar_bd(self.tabla_d, self.columnas_d, self.condicion)

        self.carac = Tabla()
        conc = con.copy()
        conc['db'] = self.bd_c
        self.carac.coneccion = (conc)
        self.carac.seleccionar_bd(self.tabla_c, self.columnas_c, self.condicion)
        if self.carac:
            self.carac[0]['ultima_fecha_actualisacion'] = time.strftime(formato_fecha_hora, self.carac[0]['ultima_fecha_actualisacion'])
            self.sincronisar('formulario', 0, self.carac, 'none_a_str')

        self.cant = Tabla()
        coni = con.copy()
        coni['db'] = self.bd_i
        self.cant.coneccion = (coni)
        self.cant.seleccionar_bd(self.tabla_i, self.columnas_i, self.condicion)
        if self.cant:
            #for i in self.diagnosticos:
                #marco_a.insert('', 'end', text=i['nombre'])
            self.sincronisar('formulario', 0, self.cant, 'none_a_str')

    def _gui(self):
        Ventana.__init__(self, self.master, self.archivo_gui)


