<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Diseño deberia ser compatible con US-LETTER: 215.9mm x 279.4mm -->
<xsl:variable name="alto_pagina" select="279.4" />
<xsl:variable name="ancho_pagina" select="215.9" />

<xsl:variable name="borde">1px solid</xsl:variable>
<xsl:variable name="separador" select="2" />

<xsl:variable name="letra_grande">10pt</xsl:variable>
<xsl:variable name="letra_mediana">8pt</xsl:variable>
<xsl:variable name="letra_normal">4pt</xsl:variable>
<xsl:variable name="letra_pequeña">3pt</xsl:variable>


<xsl:variable name="minusculas" select="'abcdefghijklmnopqrstuvwxyz'" />
<xsl:variable name="mayusculas" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

<xsl:template match="/">
  <html>

  <head>
    <link rel="stylesheet" type="text/css" href="estilo.css"/>
    <style>
        <xsl:value-of select="$css"/>
    </style>
  </head>

  <body>
    <div class="media_pagina_ancho">

        <div class="logo" style="border:1px solid" />

        <div><h4 class="encabesado"><xsl:value-of select="reporte/tabla[@nombre='Hospitales']/fila/nombre"/></h4></div>
        <div><h4 class="encabesado"><xsl:value-of select="reporte/tabla[@nombre='Hospitales']/fila/rif"/></h4></div>

        <xsl:apply-templates select="reporte/tabla[@nombre='Pacientes']/fila"/>

        <h1 class="encabesado">Recipe</h1>

        <div class="caja principal" >
            <xsl:apply-templates select="reporte/tabla[@nombre='Medicacion_Activa']"/>
        </div>

        <table>
            <tr>
                <td>
                    <xsl:apply-templates select="reporte/tabla[@nombre='Hospitales']/fila"/>
                </td>

                <td>
                    <xsl:apply-templates select="reporte/tabla[@nombre='Medicos']/fila"/>
                </td>
            </tr>
        </table>

    </div>
  </body>
  </html>
</xsl:template>


<xsl:template match="tabla[@nombre='Pacientes']/fila">
    <div class="caja nombre">

        <div >
        <h4 class="encabesado">Paciente</h4>
        &#160;
        <xsl:value-of select="nombre"/>
        </div>

        <div>
        <h4 class="encabesado">C.I.</h4>
        &#160;
        <span><xsl:value-of select="ci"/></span>
        </div>

        <div>
        <h4 class="encabesado">Fecha de Nacimiento</h4>
        &#160;
        <span><xsl:value-of select="fecha_nacimiento"/></span>
        </div>

        <div>
        <h4 class="encabesado">Telefono</h4>
        &#160;
        <span><xsl:value-of select="telefono"/></span>
        </div>

        <xsl:if test="not(normalize-space(direccion) = '')">
        <div>
        <h4 class="encabesado">Dirección</h4>
        &#160;
        <xsl:value-of select="direccion"/>
        </div>
        </xsl:if>

    </div>

</xsl:template>


<xsl:template match="tabla[@nombre='Hospitales']/fila">
    <div class="espaciado">

        <div>
            <h4 class="encabesado">Telefnono:</h4>
            &#160;
            <xsl:value-of select="telefono"/>
        </div>

        <div>
            <h4 class="encabesado">Dirección:</h4>
            &#160;
            <xsl:value-of select="direccion"/>
        </div>

        <div>
            <h4 class="encabesado">Fecha:</h4>
            &#160;
            <xsl:value-of select="//@fecha"/>
        </div>

    </div>
</xsl:template>


<xsl:template match="tabla[@nombre='Medicacion_Activa']">

      <xsl:for-each select="fila">
        <div class="linea">

        <h3 class="encabesado" ><xsl:value-of select="medicamento"/></h3>

        </div>
      </xsl:for-each>

</xsl:template>


<xsl:template match="tabla[@nombre='Medicos']/fila">
    <div style="margin:10mm;">

        <div style="
        border-top:1px solid;
        width:25ex;
        margin:auto;
        "/>

        <h4 class="encabesado"><xsl:value-of select="nombre"/></h4>
        <br/>
        <h4 class="encabesado"><xsl:value-of select="ci"/></h4>


    </div>
</xsl:template>


</xsl:stylesheet>
