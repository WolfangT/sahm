<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="estilo_estandar.xsl"/>

<xsl:template match="/">
<html>

    <head>
        <link rel="stylesheet" type="text/css" href="estilo.css"/>
        <style>
            <xsl:value-of select="$css"/>
        </style>
    </head>

    <body>
        <div class="pagina">
            <div class="wrapper">
                <table class="mostrador">
                    <tr>
                        <td>
                            <img src="./../Recursos/logo.gif" class="logo" />
                        </td>
                        <td>
                            <xsl:apply-templates select="reporte/tabla[@nombre='Hospitales']"/>
                        </td>
                    </tr>
                </table>

                <xsl:apply-templates select="reporte/tabla[@nombre='Personas']"/>
                <xsl:apply-templates select="reporte/tabla[@nombre='Caracteristicas']"/>
                <xsl:apply-templates select="reporte/tabla[@nombre='Contacto']"/>

                <h5 class="titulo">Historia Medica</h5>

                <div class="principal">

                    <xsl:apply-templates select="reporte/tabla[@nombre='Historial']"/>

                    <xsl:apply-templates select="reporte/tabla[@nombre='Consultas']"/>

                </div>

                <div class="push"/>
            </div>

            <div class="footer">

                <xsl:apply-templates select="reporte/tabla[@nombre='Medicos']"/>
            </div>

        </div>
    </body>

</html>
</xsl:template>


<xsl:template match="tabla[@nombre='Hospitales']">
    <div class="caja">

<!--
        <div class="centrado"><h4 class="encabesado"><xsl:value-of select="fila/nombre"/></h4></div>
-->

        <div>
            <h4 class="encabesado">R.I.F.:</h4>
            &#160;
            <xsl:value-of select="fila/rif"/>
        </div>

        <div>
            <h4 class="encabesado">Telefono:</h4>
            &#160;
            <xsl:value-of select="fila/telefono"/>
        </div>

        <div>
            <h4 class="encabesado">Dirección:</h4>
            &#160;
            <xsl:value-of select="fila/direccion"/>
        </div>

        <div>
            <h4 class="encabesado">Fecha:</h4>
            &#160;
            <xsl:value-of select="//@fecha"/>
        </div>

    </div>
</xsl:template>

<xsl:template match="tabla[@nombre='Medicos']">
    <div class="espacio_firma centrado irrompible">

        <div class="firma"/>

        <div>
            <h4 class="encabesado"><xsl:value-of select="fila/nombre"/></h4>
        </div>

        <div>
            <h4 class="encabesado">C.I.:</h4>
            &#160;
            <xsl:value-of select="fila/ci"/>
        </div>

        <div>
            <h4 class="encabesado">Especialista en</h4>
            &#160;
            <xsl:value-of select="fila/especialidad"/>
        </div>

    </div>
</xsl:template>

<xsl:template match="tabla[@nombre='Personas']">
    <xsl:for-each select="fila">
        <div class="enmarcado caja">

            <table class="mostrador">
                <tr>
                    <td colspan="2">
                        <h4 class="encabesado">Paciente:</h4>
                        &#160;
                        <xsl:value-of select="nombre"/>
                    </td>
                    <td colspan="2">
                        <h4 class="encabesado">C.I.:</h4>
                        &#160;
                        <xsl:value-of select="ci_tipo"/>-<xsl:value-of select="ci_numero"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="encabesado">Sexo:</h4>
                        &#160;
                        <xsl:value-of select="sexo"/>
                    </td>
                    <td>
                        <h4 class="encabesado">Grupo Sangineo:</h4>
                        &#160;
                        <xsl:value-of select="abo"/>
                        <xsl:if test="rh = 'True'">+</xsl:if>
                        <xsl:if test="rh = 'False'">-</xsl:if>
                    </td>
                    <td>
                        <h4 class="encabesado">Fecha de Nacimiento:</h4>
                        &#160;
                        <xsl:value-of select="fecha_nacimiento"/>
                    </td>

                </tr>
            </table>

        </div>
    </xsl:for-each>
</xsl:template>


<xsl:template match="tabla[@nombre='Caracteristicas']">
    <xsl:for-each select="fila">
        <div class="enmarcado caja">
            <ul>
                <li>Talla:&#160;<xsl:value-of select="talla"/>&#160;cm</li>
                <li>Peso:&#160;<xsl:value-of select="peso"/>&#160;Kg</li>
                <li>Ocupacion:&#160;<xsl:value-of select="ocupacion"/></li>
            </ul>
            <xsl:if test="habitos_alimenticios != ''">
                <li>Habitos Alimenticios</li>
                <dl>
                <dd><xsl:value-of select="habitos_alimenticios"/></dd>
                </dl>
            </xsl:if>
            <xsl:if test="habitos_psicologicos != ''">
                <li>Habitos Psicologicos</li>
                <dl>
                <dd><xsl:value-of select="habitos_psicologicos"/></dd>
                </dl>
            </xsl:if>
            <xsl:if test="estilo_vida != ''">
                <li>Estilo Vida</li>
                <dl>
                <dd><xsl:value-of select="estilo_vida"/></dd>
                </dl>
            </xsl:if>
            <xsl:if test="notas != ''">
                <li>Notas</li>
                <dl>
                <dd><xsl:value-of select="notas"/></dd>
                </dl>
            </xsl:if>
        </div>
    </xsl:for-each>
</xsl:template>

<xsl:template match="tabla[@nombre='Contacto']">
    <xsl:for-each select="fila">
        <div class="enmarcado caja">
            <ul>
                <li>Telefono:&#160;<xsl:value-of select="telefono_codigo"/>-<xsl:value-of select="telefono_numero"/></li>
                <li>Correo Electronico:&#160;<xsl:value-of select="correo_electronico_usuario"/>@<xsl:value-of select="correo_electronico_dominio"/></li>
                <li>Dirección:</li>
                <dl>
                    <dd><xsl:apply-templates select="estado"/>&#160;-&#160;<xsl:apply-templates select="ciudad"/></dd>
                    <dd><xsl:apply-templates select="direccion"/></dd>
                </dl>
            </ul>
        </div>
    </xsl:for-each>
</xsl:template>


<xsl:template match="tabla[@nombre='Consultas']">
    <xsl:if test="fila">
    <div >
    <p class="sub_titulo">Se han realizado las siguientes consultas:</p>
    <ul>
        <xsl:for-each select="fila">
            <div class="linea">
                <li>
                    <span class="importante"><xsl:apply-templates select="fecha"/></span>
                    <dl>
                        <xsl:if test="motivo != ''">
                        <dt>Motivo</dt>
                        <dd><xsl:value-of select="motivo"/></dd>
                        </xsl:if>

                        <xsl:if test="notas != ''">
                        <dt>Notas</dt>
                        <dd><xsl:value-of select="notas"/></dd>
                        </xsl:if>
                    </dl>
                </li>
            </div>
        </xsl:for-each>
    </ul>
    </div>
    </xsl:if>
</xsl:template>


<xsl:template match="tabla[@nombre='Historial']">
    <xsl:if test="fila">
    <div >
    <p class="sub_titulo">Durante toda su historia:</p>
    <ul>
        <xsl:for-each select="fila">
            <div class="linea">
                <li>
                    <span class="importante"><xsl:value-of select="descripcion"/></span>
                    <dl>
                        <dt>Fecha</dt>
                        <dd><xsl:apply-templates select="fecha"/></dd>
                    </dl>
                </li>
            </div>
        </xsl:for-each>
    </ul>
    </div>
    </xsl:if>
</xsl:template>



<xsl:template match="tabla[@nombre='Plan_Tratamientos_Farmacologicos']">
    <xsl:if test="fila">
    <div >
    <p class="sub_titulo">Se recetaron los siguientes medicamentos:</p>
    <dl>
        <xsl:for-each select="fila">
            <li>
                <span class="importante"><xsl:value-of select="medicamento"/></span>
                &#160;
                <xsl:value-of select="dosis"/>
                <xsl:value-of select="unidad"/>
            </li>
        </xsl:for-each>
    </dl>
    </div>
    </xsl:if>
</xsl:template>

<xsl:template match="tabla[@nombre='Plan_Tratamientos_No_Farmacologicos']">
    <xsl:if test="fila">
    <div>
    <p class="sub_titulo">Se sugirieron los siguientes tratamientos:</p>
    <dl>
        <xsl:for-each select="fila">
            <li class="importante"><xsl:value-of select="nombre"/></li>
        </xsl:for-each>
    </dl>
    </div>
    </xsl:if>
</xsl:template>

<xsl:template match="tabla[@nombre='Sintomas']">
    <xsl:if test="fila">
    <div>
    <p class="sub_titulo">El paciente presento los siguientes síntomas:</p>
    <dl>
        <xsl:for-each select="fila">
            <li class="importante"><xsl:value-of select="nombre"/></li>
            <dd><xsl:value-of select="descripcion"/></dd>
        </xsl:for-each>
    </dl>
    </div>
    </xsl:if>
</xsl:template>

<xsl:template match="tabla[@nombre='Signos']">
    <xsl:if test="fila">
    <div>
    <p class="sub_titulo">Se encontraron los siguientes signos clínicos:</p>
    <dl>
        <xsl:for-each select="fila">
            <li class="importante"><xsl:value-of select="nombre"/></li>
            <dd><xsl:value-of select="descripcion"/></dd>
        </xsl:for-each>
    </dl>
    </div>
    </xsl:if>
</xsl:template>

<xsl:template match="tabla[@nombre='Diagnosticos']">
    <xsl:if test="fila">
    <div>
    <p class="sub_titulo">Se formularon los siguientes diagnósticos:</p>
    <dl>
        <xsl:for-each select="fila">
            <li class="importante"><xsl:value-of select="nombre"/></li>
            <dd><xsl:value-of select="descripcion"/></dd>
            <dd><xsl:value-of select="pronostico"/></dd>
        </xsl:for-each>
    </dl>
    </div>
    </xsl:if>
</xsl:template>

<xsl:template match="fecha">
    <span>
        &#160;
        <xsl:value-of select="dia"/>/<xsl:value-of select="mes"/>/<xsl:value-of select="año"/>
        &#160;
        <xsl:value-of select="hora"/>:<xsl:value-of select="minuto"/>:<xsl:value-of select="segundo"/>
    </span>
</xsl:template>

<!--
<xsl:template match="notas" name="split">
    <xsl:param name="pText" select="."/>
    <p>
        <xsl:if test="string-length($pText)">
            <xsl:if test="not($pText=.)">
                <br />
            </xsl:if>
            <xsl:value-of select="substring-before(concat($pText,'
'),'\
')"/>
            <xsl:call-template name="split">
                <xsl:with-param name="pText" select="substring-after($pText, '\
')"/>
            </xsl:call-template>
        </xsl:if>
    </p>

 </xsl:template>
-->

</xsl:stylesheet>
