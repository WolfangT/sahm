<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="estilo_estandar.xsl"/>
<xsl:variable name="alto_pagina" select="279.4 div 2" />

<xsl:template match="/">
  <html>

  <head>
    <link rel="stylesheet" type="text/css" href="estilo.css"/>
    <style>
        <xsl:value-of select="$css"/>
    </style>
  </head>

  <body>

    <div class="pagina">
        <div class="wrapper">
            <table class="mostrador">
                <tr>
                    <td>
                        <img src="./../Recursos/logo.gif" class="logo" />
                    </td>
                    <td>
                        <xsl:apply-templates select="reporte/tabla[@nombre='Hospitales']/fila"/>
                    </td>
                </tr>
            </table>

            <xsl:apply-templates select="reporte/tabla[@nombre='Pacientes']"/>

            <h5 class="titulo">Recipe</h5>

            <div class="principal" >
                <xsl:apply-templates select="reporte/tabla[@nombre='Medicacion_Activa']" mode="recipe"/>
            </div>

            <div class="push"/>

        </div>

        <div class="footer">
            <xsl:apply-templates select="reporte/tabla[@nombre='Medicos']"/>
        </div>

    </div>

    <div class="pagina">

        <div class="wrapper">
            <table class="mostrador">
                <tr>
                    <td>
                        <img src="./../Recursos/logo.gif" class="logo" />
                    </td>
                    <td>
                        <xsl:apply-templates select="reporte/tabla[@nombre='Hospitales']/fila"/>
                    </td>
                </tr>
            </table>

            <xsl:apply-templates select="reporte/tabla[@nombre='Pacientes']"/>

            <h5 class="titulo">Indicaciones</h5>

            <div class="principal" >
                <xsl:apply-templates select="reporte/tabla[@nombre='Medicacion_Activa']" mode="indicacion"/>
                <xsl:apply-templates select="reporte/tabla[@nombre='Tratamientos_Activos']" mode="indicacion"/>
            </div>

            <div class="push"/>

        </div>

        <div class="footer">
            <xsl:apply-templates select="reporte/tabla[@nombre='Medicos']"/>
        </div>
    </div>


  </body>
  </html>
</xsl:template>


<xsl:template match="tabla[@nombre='Pacientes']">
    <xsl:for-each select="fila">
        <div class="enmarcado caja">

            <table class="mostrador">
                <tr>
                    <td colspan="2">
                        <h4 class="encabesado">Paciente:</h4>
                        &#160;
                        <xsl:value-of select="nombre"/>
                    </td>
                    <td colspan="2">
                        <h4 class="encabesado">C.I.:</h4>
                        &#160;
                        <xsl:value-of select="ci"/>
                    </td>
                </tr>
                <tr>
<!--
                    <td>
                        <h4 class="encabesado">Sexo:</h4>
                        &#160;
                        <xsl:value-of select="sexo"/>
                    </td>
                    <td>
                        <h4 class="encabesado">Grupo Sangineo:</h4>
                        &#160;
                        <xsl:value-of select="tipo_sangre"/>
                    </td>
-->
                    <td>
                        <h4 class="encabesado">Edad:</h4>
                        &#160;
                        <xsl:value-of select="edad"/>
                    </td>
                    <td>
                        <h4 class="encabesado">Fecha de Nacimiento:</h4>
                        &#160;
                        <xsl:value-of select="fecha_nacimiento"/>
                    </td>

                </tr>
            </table>

        </div>
    </xsl:for-each>
</xsl:template>


<xsl:template match="tabla[@nombre='Hospitales']/fila">
    <div >

        <!--
        <div>
            <h4 class="encabesado"><xsl:value-of select="nombre"/></h4>
        </div>
        -->


        <div>
            <h4 class="encabesado">R.I.F.:</h4>
            &#160;
            <xsl:value-of select="rif"/>
        </div>

        <div>
            <h4 class="encabesado">Telefono:</h4>
            &#160;
            <xsl:value-of select="telefono"/>
        </div>

        <div>
            <h4 class="encabesado">Dirección:</h4>
            &#160;
            <xsl:value-of select="direccion"/>
        </div>

        <div>
            <h4 class="encabesado">Fecha:</h4>
            &#160;
            <xsl:value-of select="//@fecha"/>
        </div>

    </div>
</xsl:template>


<xsl:template match="tabla[@nombre='Medicos']">
    <div class="espacio_firma centrado irrompible">

        <div class="firma"/>

        <div>
            <h4 class="encabesado"><xsl:value-of select="fila/nombre"/></h4>
        </div>

        <div>
            <h4 class="encabesado">C.I.:</h4>
            &#160;
            <xsl:value-of select="fila/ci"/>
        </div>

        <div>
            <h4 class="encabesado">Especialista en</h4>
            &#160;
            <xsl:value-of select="fila/especialidad"/>
        </div>

    </div>
</xsl:template>


<xsl:template match="tabla[@nombre='Medicacion_Activa']" mode="recipe">

    <xsl:for-each select="fila">
        <div class="linea">

            <span class="importante"><xsl:value-of select="medicamento"/></span>

        </div>
    </xsl:for-each>
</xsl:template>

<xsl:template match="tabla[@nombre='Medicacion_Activa']" mode="indicacion">

    <div class="irrompible">

      <xsl:for-each select="fila">

        <div class="linea">


            <h4 class="encabesado"><xsl:value-of select="medicamento"/>:</h4>
            &#160;
            <xsl:value-of select="format-number(dosis, '########.########')"/>
            &#160;
            <xsl:value-of select="unidad"/>

            <xsl:if test="not(normalize-space(horario) = 'None')">
                &#160;
                <xsl:value-of select="horario"/>
            </xsl:if>

            <xsl:if test="not(normalize-space(duracion) = 'None')">
                &#160;
                <xsl:value-of select="duracion"/>
            </xsl:if>

            <xsl:if test="not(normalize-space(notas) = '')">
                &#160;
                <h4 class="encabesado">Notas:</h4>
                &#160;
                <xsl:value-of select="notas"/>
            </xsl:if>

        </div>

      </xsl:for-each>

    </div>

</xsl:template>


<xsl:template match='tabla[@nombre="Tratamientos_Activos"]' mode="indicacion">

    <div class="irrompible">

      <xsl:for-each select="fila">

        <div class="linea">

            <h4 class="encabesado"><xsl:value-of select="nombre"/></h4>
            &#160;
            <xsl:value-of select="descripcion"/>

            <xsl:if test="not(normalize-space(duracion) = 'None')">
                &#160;
                <xsl:value-of select="duracion"/>
            </xsl:if>

        </div>

      </xsl:for-each>

    </div>


</xsl:template>

</xsl:stylesheet>

