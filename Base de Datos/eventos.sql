-- -----------------------------------------------------
-- EVENT ACTUALIZAR_MEDICAMENTOS
-- -----------------------------------------------------

USE `Cardiologia`;
DROP event IF EXISTS `Cardiologia`.`ACTUALIZAR_MEDICAMENTOS`;

DELIMITER $$

CREATE EVENT `ACTUALIZAR_MEDICAMENTOS`
ON SCHEDULE EVERY 1 DAY
ON COMPLETION NOT PRESERVE ENABLE
DO 
UPDATE `Cardiologia`.`Plan_Medicacion`
SET `activo` = 0
WHERE `activo` = 1 AND CURDATE() >= DATE(`fecha_terminacion`);$$

DELIMITER ;

-- -----------------------------------------------------
-- EVENT ACTUALIZAR_TERAPIAS
-- -----------------------------------------------------

USE `Cardiologia`;
DROP event IF EXISTS `Cardiologia`.`ACTUALIZAR_TERAPIAS`;

DELIMITER $$

CREATE EVENT `ACTUALIZAR_TERAPIAS`
ON SCHEDULE EVERY 1 DAY
ON COMPLETION NOT PRESERVE ENABLE
DO 
UPDATE `Cardiologia`.`Plan_Terapias`
SET `activo` = 0
WHERE `activo` = 1 AND CURDATE() >= DATE(`fecha_terminacion`);$$

DELIMITER ;
