USE `Semiologia`$$
DROP TRIGGER IF EXISTS `Semiologia`.`Ex_Fisicos_AINS` $$
USE `Semiologia`$$


CREATE TRIGGER `Ex_Fisicos_AINS` AFTER INSERT ON `Ex_Fisicos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Semiologia',
'Ex_Fisicos',
'Examen Fisico',
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$



USE `Semiologia`$$
DROP TRIGGER IF EXISTS `Semiologia`.`Ex_ECG_AINS` $$
USE `Semiologia`$$


CREATE TRIGGER `Ex_ECG_AINS` AFTER INSERT ON `Ex_ECG` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Semiologia',
'Ex_ECG',
'ElectroCardioGrama',
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$



USE `Semiologia`$$
DROP TRIGGER IF EXISTS `Semiologia`.`Ex_Especiales_BINS` $$
USE `Semiologia`$$


CREATE TRIGGER `Ex_Especiales_BINS` BEFORE INSERT ON `Ex_Especiales` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.clase = `General`.CAP_FIRST(NEW.clase);
SET NEW.tipo = `General`.CAP_FIRST(NEW.tipo);
END;$$


USE `Semiologia`$$
DROP TRIGGER IF EXISTS `Semiologia`.`Ex_Especiales_AINS` $$
USE `Semiologia`$$


CREATE TRIGGER `Ex_Especiales_AINS` AFTER INSERT ON `Ex_Especiales` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Semiologia',
'Ex_Especiales',
CONCAT('Examen Especial: ',NEW.clase, ', ', NEW.tipo),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$



USE `Semiologia`$$
DROP TRIGGER IF EXISTS `Semiologia`.`Sintomas_BINS` $$
USE `Semiologia`$$


CREATE TRIGGER `Sintomas_BINS` BEFORE INSERT ON `Sintomas` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
SET NEW.descripcion = `General`.CAP_FIRST(NEW.descripcion);
END;$$


USE `Semiologia`$$
DROP TRIGGER IF EXISTS `Semiologia`.`Sintomas_AINS` $$
USE `Semiologia`$$


CREATE TRIGGER `Sintomas_AINS` AFTER INSERT ON `Sintomas` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Semiologia',
'Sintomas',
CONCAT('Sintoma: ', NEW.nombre),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$


USE `Semiologia`$$
DROP TRIGGER IF EXISTS `Semiologia`.`Sintomas_AUPD` $$
USE `Semiologia`$$


CREATE TRIGGER `Sintomas_AUPD` AFTER UPDATE ON `Sintomas` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Semiologia',
'Sintomas',
CONCAT('Sintoma: ', NEW.nombre),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha_terminacion`
);

END;$$



USE `Semiologia`$$
DROP TRIGGER IF EXISTS `Semiologia`.`Signos_BINS` $$
USE `Semiologia`$$


CREATE TRIGGER `Signos_BINS` BEFORE INSERT ON `Signos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
SET NEW.descripcion = `General`.CAP_FIRST(NEW.descripcion);
END;$$


USE `Semiologia`$$
DROP TRIGGER IF EXISTS `Semiologia`.`Signo_AINS` $$
USE `Semiologia`$$


CREATE TRIGGER `Signo_AINS` AFTER INSERT ON `Signos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Semiologia',
'Signos',
CONCAT('Signo Clinico: ', NEW.nombre),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$



USE `General`$$
DROP TRIGGER IF EXISTS `General`.`Consultas_BINS` $$
USE `General`$$


CREATE TRIGGER `Consultas_BINS` BEFORE INSERT ON Consultas FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.asistida = IF(curdate()>NEW.fecha, True, False);
SET NEW.motivo = `General`.CAP_FIRST(NEW.motivo);
END;$$



USE `Tratamientos`$$
DROP TRIGGER IF EXISTS `Tratamientos`.`TratamientosF_BINS` $$
USE `Tratamientos`$$


CREATE TRIGGER `TratamientosF_BINS` BEFORE INSERT ON `Plan_Tratamientos_Farmacologicos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.medicamento = `General`.CAP_FIRST(NEW.medicamento);
SET NEW.unidad = `General`.CAP_FIRST(NEW.unidad);
END;$$


USE `Tratamientos`$$
DROP TRIGGER IF EXISTS `Tratamientos`.`TratamientosF_AINS` $$
USE `Tratamientos`$$


CREATE TRIGGER `TratamientosF_AINS` AFTER INSERT ON `Plan_Tratamientos_Farmacologicos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Tratamientos',
'Plan_Tratamientos_Farmacologicos',
CONCAT('Medicamento: ', NEW.Medicamento),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$


USE `Tratamientos`$$
DROP TRIGGER IF EXISTS `Tratamientos`.`TratamientosF_AUPD` $$
USE `Tratamientos`$$


CREATE TRIGGER `TratamientosF_AUPD` AFTER UPDATE ON `Plan_Tratamientos_Farmacologicos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Tratamientos',
'Plan_Tratamientos_Farmacologicos',
CONCAT('Detener Medicamento: ', NEW.Medicamento),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NOW()
);

END;$$



USE `Tratamientos`$$
DROP TRIGGER IF EXISTS `Tratamientos`.`TratamientosNF_BINS` $$
USE `Tratamientos`$$


CREATE TRIGGER `TratamientosNF_BINS` BEFORE INSERT ON `Plan_Tratamientos_No_Farmacologicos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
END;$$


USE `Tratamientos`$$
DROP TRIGGER IF EXISTS `Tratamientos`.`TratamientosNF_AINS` $$
USE `Tratamientos`$$


CREATE TRIGGER `TratamientosNF_AINS` AFTER INSERT ON `Plan_Tratamientos_No_Farmacologicos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Tratamientos',
'Plan_Tratamientos_No_Farmacologicos',
CONCAT('Tratamiento: ', NEW.nombre),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$


USE `Tratamientos`$$
DROP TRIGGER IF EXISTS `Tratamientos`.`TratamientosNF_AUPD` $$
USE `Tratamientos`$$


CREATE TRIGGER `TratamientosNF_AUPD` AFTER UPDATE ON `Plan_Tratamientos_No_Farmacologicos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Tratamientos',
'Plan_Tratamientos_No_Farmacologicos',
CONCAT('Detener Tratamiento: ', NEW.Nombre),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NOW()
);

END;$$



USE `Tratamientos`$$
DROP TRIGGER IF EXISTS `Tratamientos`.`PlanDiagnostico_AINS` $$
USE `Tratamientos`$$


CREATE TRIGGER `PlanDiagnostico_AINS` AFTER INSERT ON `Plan_Diagnostico` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Tratamientos',
'Plan_Diagnostico',
CONCAT('Solicitud de Estudio: ', NEW.examen),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$


USE `Tratamientos`$$
DROP TRIGGER IF EXISTS `Tratamientos`.`PlanDiagnostico_AUPD` $$
USE `Tratamientos`$$


CREATE TRIGGER `PlanDiagnostico_AUPD` AFTER UPDATE ON `Plan_Diagnostico` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Tratamientos',
'Plan_Diagnostico',
CONCAT('Entrega de Estudio: ', NEW.examen),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NOW()
);

END;$$


USE `Tratamientos`$$
DROP TRIGGER IF EXISTS `Tratamientos`.`PlanDiagnostico_BINS` $$
USE `Tratamientos`$$


CREATE TRIGGER `PlanDiagnostico_BINS` BEFORE INSERT ON `Plan_Diagnostico` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.Examen = `General`.CAP_FIRST(NEW.examen);
END;$$



USE `Hospitales`$$
DROP TRIGGER IF EXISTS `Hospitales`.`Especialistas_BINS` $$
USE `Hospitales`$$


CREATE TRIGGER `Especialistas_BINS` BEFORE INSERT ON Especialistas FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.ci_tipo = `General`.CAP_FIRST(NEW.CI_tipo);
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
SET NEW.especialidad = `General`.CAP_FIRST(NEW.especialidad);
END;$$



USE `Hospitales`$$
DROP TRIGGER IF EXISTS `Hospitales`.`Centros_Medicos_BINS` $$
USE `Hospitales`$$


CREATE TRIGGER `Centros_Medicos_BINS` BEFORE INSERT ON `Centros_Medicos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.rif_tipo = `General`.CAP_FIRST(NEW.rif_tipo);
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
END;$$



USE `Pacientes`$$
DROP TRIGGER IF EXISTS `Pacientes`.`Diagnosticos_BINS` $$
USE `Pacientes`$$


CREATE TRIGGER `Diagnosticos_BINS` BEFORE INSERT ON Diagnosticos FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
SET NEW.descripcion = `General`.CAP_FIRST(NEW.descripcion);
SET NEW.pronostico = `General`.CAP_FIRST(NEW.pronostico);
END;$$


USE `Pacientes`$$
DROP TRIGGER IF EXISTS `Pacientes`.`Diagnosticos_AUPD` $$
USE `Pacientes`$$


CREATE TRIGGER `Diagnosticos_AUPD` AFTER UPDATE ON `Diagnosticos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Pacientes',
'Diagnosticos',
CONCAT(CASE NEW.`activo` WHEN 1 THEN 'Diagnostico' WHEN 0 THEN 'Antecedente' END, ': ', NEW.Nombre),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha_terminacion`
);

END;$$


USE `Pacientes`$$
DROP TRIGGER IF EXISTS `Pacientes`.`Diagnosticos_BUPD` $$
USE `Pacientes`$$


CREATE TRIGGER `Diagnosticos_BUPD` BEFORE UPDATE ON `Diagnosticos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.fecha_terminacion = NOW();
END;$$


USE `Pacientes`$$
DROP TRIGGER IF EXISTS `Pacientes`.`Diagnosticos_AINS` $$
USE `Pacientes`$$


CREATE TRIGGER `Diagnosticos_AINS` AFTER INSERT ON `Diagnosticos` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Pacientes',
'Diagnosticos',
CONCAT(CASE NEW.`activo` WHEN 1 THEN 'Diagnostico' WHEN 0 THEN 'Antecedente' END, ': ', NEW.Nombre),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$



USE `Pacientes`$$
DROP TRIGGER IF EXISTS `Pacientes`.`Personas_BINS` $$
USE `Pacientes`$$


CREATE TRIGGER `Personas_BINS` BEFORE INSERT ON Personas FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
END;$$



USE `Pacientes`$$
DROP TRIGGER IF EXISTS `Pacientes`.`Contacto_BINS` $$
USE `Pacientes`$$


CREATE TRIGGER `Contacto_BINS` BEFORE INSERT ON Contacto FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.direccion = `General`.CAP_FIRST(NEW.direccion);
SET NEW.estado = `General`.CAP_FIRST(NEW.estado);
SET NEW.ciudad = `General`.CAP_FIRST(NEW.ciudad);
END;$$
