DROP SCHEMA IF EXISTS `Semiologia` ;
CREATE SCHEMA IF NOT EXISTS `Semiologia` DEFAULT CHARACTER SET utf8 ;
DROP SCHEMA IF EXISTS `General` ;
CREATE SCHEMA IF NOT EXISTS `General` DEFAULT CHARACTER SET utf8 ;
DROP SCHEMA IF EXISTS `Tratamientos` ;
CREATE SCHEMA IF NOT EXISTS `Tratamientos` DEFAULT CHARACTER SET utf8 ;
DROP SCHEMA IF EXISTS `Hospitales` ;
CREATE SCHEMA IF NOT EXISTS `Hospitales` ;
DROP SCHEMA IF EXISTS `Pacientes` ;
CREATE SCHEMA IF NOT EXISTS `Pacientes` ;
USE `Semiologia` ;


-- -----------------------------------------------------
-- Table `Pacientes`.`Personas_Especialistas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pacientes`.`Personas_Especialistas` ;

CREATE  TABLE IF NOT EXISTS `Pacientes`.`Personas_Especialistas` (
  `idPersonas` INT UNSIGNED NOT NULL ,
  `idMedicos` INT UNSIGNED NOT NULL ,
  `fecha_asignacion` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  INDEX `fk_Personas_Especialistas_1` (`idMedicos` ASC) ,
  INDEX `fk_Personas_Especialistas_2` (`idPersonas` ASC) ,
  PRIMARY KEY (`idPersonas`, `idMedicos`) ,
  CONSTRAINT `fk_Personas_Especialistas_1`
    FOREIGN KEY (`idMedicos` )
    REFERENCES `Hospitales`.`Especialistas` (`idMedicos` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Personas_Especialistas_2`
    FOREIGN KEY (`idPersonas` )
    REFERENCES `Pacientes`.`Personas` (`idPersonas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Guarda los medicos tratantes de un paciente, asi como la fec' /* comment truncated */;

-- -----------------------------------------------------
-- Table `Hospitales`.`Especialistas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Hospitales`.`Especialistas` ;

CREATE  TABLE IF NOT EXISTS `Hospitales`.`Especialistas` (
  `idMedicos` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `Usuario` VARCHAR(16) NOT NULL ,
  `nombre` VARCHAR(50) NOT NULL DEFAULT 'John Doe' ,
  `especialidad` VARCHAR(50) NOT NULL DEFAULT '' ,
  `ci_tipo` CHAR(1) NOT NULL DEFAULT 'V' ,
  `ci_numero` DECIMAL(8,0) UNSIGNED NOT NULL DEFAULT 0 ,
  `sexo` TINYINT(3) UNSIGNED NULL DEFAULT 0 COMMENT '0 = Indefinido\n1 = Masculinio\n2 = Femenino' ,
  `fecha_nacimiento` DATE NULL DEFAULT NULL ,
  PRIMARY KEY (`idMedicos`, `Usuario`) ,
  UNIQUE INDEX `ci_numero_UNIQUE` (`ci_tipo` ASC, `ci_numero` ASC) )
ENGINE = InnoDB
COMMENT = 'Tabla con la información general de un Medico.';


-- -----------------------------------------------------
-- Table `Pacientes`.`Personas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pacientes`.`Personas` ;

CREATE  TABLE IF NOT EXISTS `Pacientes`.`Personas` (
  `idPersonas` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(50) NULL DEFAULT NULL ,
  `ci_tipo` CHAR(1) NULL DEFAULT 'V' ,
  `ci_numero` DECIMAL(8,0) UNSIGNED NULL ,
  `fecha_nacimiento` DATE NULL DEFAULT NULL ,
  `sexo` TINYINT(3) UNSIGNED NULL DEFAULT 0 COMMENT '0 = Indefinido\n1 = Masculinio\n2 = Femenino' ,
  `abo` TINYINT(2) UNSIGNED NULL DEFAULT NULL COMMENT '0 = A\n1 = B\n2 = AB\n3 = O' ,
  `rh` TINYINT(1) NULL DEFAULT NULL COMMENT '0=Negativo\n1=Positivo' ,
  PRIMARY KEY (`idPersonas`) ,
  UNIQUE INDEX `ci_numero_UNIQUE` (`ci_numero` ASC) )
ENGINE = InnoDB
COMMENT = 'Tabla con la información general de un paciente.';


-- -----------------------------------------------------
-- Table `Hospitales`.`Centros_Medicos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Hospitales`.`Centros_Medicos` ;

CREATE  TABLE IF NOT EXISTS `Hospitales`.`Centros_Medicos` (
  `idHospitales` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(50) NOT NULL ,
  `rif_tipo` CHAR(1) NOT NULL DEFAULT 'J' ,
  `rif_numero` DECIMAL(8,0) UNSIGNED NOT NULL DEFAULT 0 ,
  `rif_final` DECIMAL(1,0) UNSIGNED NULL DEFAULT 0 ,
  `direccion` MEDIUMTEXT NULL ,
  `telefono_codigo` DECIMAL(4,0) NULL DEFAULT NULL ,
  `telefono_numero` DECIMAL(7,0) NULL DEFAULT NULL ,
  PRIMARY KEY (`idHospitales`, `rif_numero`) ,
  UNIQUE INDEX `rif_numero_UNIQUE` (`rif_numero` ASC) ,
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) )
ENGINE = InnoDB
COMMENT = 'Tabla que guarda la información de los centro de atención me' /* comment truncated */;


-- -----------------------------------------------------
-- Table `General`.`Consultas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `General`.`Consultas` ;

CREATE  TABLE IF NOT EXISTS `General`.`Consultas` (
  `idConsultas` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `idMedicos` INT UNSIGNED NULL ,
  `idPersonas` INT UNSIGNED NULL ,
  `idHospitales` INT UNSIGNED NULL ,
  `motivo` VARCHAR(50) NULL DEFAULT NULL ,
  `asistida` TINYINT(1) NOT NULL DEFAULT 0 ,
  `notas` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`idConsultas`) ,
  INDEX `fk_Consultas_1` (`idMedicos` ASC) ,
  INDEX `fk_Consultas_2` (`idPersonas` ASC) ,
  INDEX `fk_Consultas_Centros_Medicos1` (`idHospitales` ASC) ,
  CONSTRAINT `fk_Consultas_1`
    FOREIGN KEY (`idMedicos` )
    REFERENCES `Hospitales`.`Especialistas` (`idMedicos` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Consultas_2`
    FOREIGN KEY (`idPersonas` )
    REFERENCES `Pacientes`.`Personas` (`idPersonas` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Consultas_Centros_Medicos1`
    FOREIGN KEY (`idHospitales` )
    REFERENCES `Hospitales`.`Centros_Medicos` (`idHospitales` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Guarda la información de una interación entre en medico y un' /* comment truncated */;


-- -----------------------------------------------------
-- Table `Semiologia`.`Ex_Fisicos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Semiologia`.`Ex_Fisicos` ;

CREATE  TABLE IF NOT EXISTS `Semiologia`.`Ex_Fisicos` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT UNSIGNED NOT NULL ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `ta_sistolica` SMALLINT(6) NULL DEFAULT 120 ,
  `ta_diastolica` SMALLINT(6) NULL DEFAULT 80 ,
  `fc` SMALLINT(6) NULL DEFAULT 80 ,
  `fr` TINYINT(4) NULL DEFAULT 16 ,
  `rscsrs` VARCHAR(100) NULL DEFAULT 'Normal' ,
  `torax` VARCHAR(100) NULL DEFAULT 'Normal' ,
  `abdomen` VARCHAR(100) NULL DEFAULT 'Normal' ,
  `extremidades` VARCHAR(100) NULL DEFAULT 'Normal' ,
  `neurologico` VARCHAR(100) NULL DEFAULT 'Normal' ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Ex_Fisicos_Consultas1` (`idConsultas` ASC) ,
  CONSTRAINT `fk_Ex_Fisicos_Consultas1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Tabla que guarda los Examenes Fisicos.';


-- -----------------------------------------------------
-- Table `Semiologia`.`Ex_ECG`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Semiologia`.`Ex_ECG` ;

CREATE  TABLE IF NOT EXISTS `Semiologia`.`Ex_ECG` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT UNSIGNED NOT NULL ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `ritmo` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '1:Sinusual\n0:No Sinusual' ,
  `fc` SMALLINT(6) NOT NULL DEFAULT 80 ,
  `qsr` DOUBLE NOT NULL DEFAULT 0.12 ,
  `pr` DOUBLE NOT NULL DEFAULT 0.025 ,
  `qt` DOUBLE NOT NULL DEFAULT 0.40 ,
  `eje` SMALLINT(6) NOT NULL DEFAULT 45 ,
  `descripcion` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Ex_ECG_Consultas1` (`idConsultas` ASC) ,
  CONSTRAINT `fk_Ex_ECG_Consultas1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Tabla que guarda los Examenes: ElectroCardioGramas.';


-- -----------------------------------------------------
-- Table `Semiologia`.`Ex_Especiales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Semiologia`.`Ex_Especiales` ;

CREATE  TABLE IF NOT EXISTS `Semiologia`.`Ex_Especiales` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT UNSIGNED NOT NULL ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `clase` VARCHAR(50) NOT NULL DEFAULT 'Laboratorio' COMMENT 'Laboratorio\nSubjetivos\nOtros' ,
  `tipo` VARCHAR(50) NOT NULL ,
  `descripcion` MEDIUMTEXT NULL DEFAULT NULL ,
  `resultado` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Ex_Especiales_Consultas` (`idConsultas` ASC) ,
  CONSTRAINT `fk_Ex_Especiales_Consultas`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Tabla que guarda los Examenes de manera muy general para pod' /* comment truncated */;


-- -----------------------------------------------------
-- Table `Semiologia`.`Sintomas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Semiologia`.`Sintomas` ;

CREATE  TABLE IF NOT EXISTS `Semiologia`.`Sintomas` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT UNSIGNED NOT NULL ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `activo` TINYINT(1) NULL DEFAULT 1 ,
  `fecha_terminacion` TIMESTAMP NULL DEFAULT NULL ,
  `nombre` VARCHAR(50) NOT NULL ,
  `descripcion` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Sintomas_Consultas1` (`idConsultas` ASC) ,
  CONSTRAINT `fk_Sintomas_Consultas1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Tabla que guarda los Sintomas reportados por el paciente.';


-- -----------------------------------------------------
-- Table `Semiologia`.`Signos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Semiologia`.`Signos` ;

CREATE  TABLE IF NOT EXISTS `Semiologia`.`Signos` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT UNSIGNED NOT NULL ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `nombre` VARCHAR(50) NOT NULL ,
  `descripcion` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Ex_Signos_Consultas1` (`idConsultas` ASC) ,
  CONSTRAINT `fk_Ex_Signos_Consultas1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Tabla que guarda los Signos Clinicos del Paciente.';

USE `General` ;

-- -----------------------------------------------------
-- Table `General`.`Historial`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `General`.`Historial` ;

CREATE  TABLE IF NOT EXISTS `General`.`Historial` (
  `idHistorial` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idPersonas` INT UNSIGNED NOT NULL ,
  `idConsultas` INT UNSIGNED NULL ,
  `fecha` TIMESTAMP NOT NULL ,
  `descripcion` VARCHAR(100) NULL DEFAULT NULL ,
  `bd` VARCHAR(100) NOT NULL ,
  `tabla` VARCHAR(100) NOT NULL COMMENT 'Examen\nTerapia\nEmergencia\nETC.' ,
  `id` INT ZEROFILL NOT NULL COMMENT 'PK del Registro orignal' ,
  INDEX `fk_Historia_Clinica_Consultas1` (`idConsultas` ASC) ,
  PRIMARY KEY (`idHistorial`) ,
  INDEX `FECHA` (`fecha` DESC) ,
  INDEX `fk_Historial_1` (`idPersonas` ASC) ,
  CONSTRAINT `fk_Historia_Clinica_Consultas1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Historial_1`
    FOREIGN KEY (`idPersonas` )
    REFERENCES `Pacientes`.`Personas` (`idPersonas` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Historial de todas las aciones realizadas a un paciente. \nPo' /* comment truncated */;


-- -----------------------------------------------------
-- Placeholder table for view `General`.`Historia_Clinica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `General`.`Historia_Clinica` (`nombre` INT, `ci` INT, `'sexo'` INT, `descripcion` INT, `'fecha_evento'` INT, `'fecha_consulta'` INT, `idConsultas` INT, `idPersonas` INT, `bd` INT, `tabla` INT, `id` INT);

-- -----------------------------------------------------
-- Placeholder table for view `General`.`Registro_Consultas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `General`.`Registro_Consultas` (`idMedicos` INT, `nombre` INT, `idConsultas` INT, `fecha` INT, `idPersonas` INT, `pacientes` INT, `motivo` INT, `notas` INT);

-- -----------------------------------------------------
-- View `General`.`Historia_Clinica`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `General`.`Historia_Clinica` ;
DROP TABLE IF EXISTS `General`.`Historia_Clinica`;
USE `General`;
CREATE OR REPLACE VIEW `General`.`Historia_Clinica` AS
SELECT
`Personas`.`nombre`,
CONCAT(`Personas`.`ci_tipo`, '-', `Personas`.`ci_numero`) AS `ci`,
CASE `Personas`.`sexo` WHEN 1 THEN 'Masculino' WHEN 2 THEN 'Femenino' ELSE 'Otro' END AS 'sexo',
`Historial`.`descripcion`,
`Historial`.`fecha` as 'fecha_evento',
`Consultas`.`fecha` as 'fecha_consulta',
`Historial`.`idConsultas`,
`Historial`.`idPersonas`,
`Historial`.`bd`,
`Historial`.`tabla`,
`Historial`.`id`
FROM `General`.`Historial`
LEFT JOIN `Pacientes`.`Personas` ON `Historial`.`idPersonas` = `Personas`.`idPersonas`
LEFT JOIN `General`.`Consultas` ON `Historial`.`idConsultas` = `Consultas`.`idConsultas`
ORDER BY `Historial`.`fecha` DESC;

-- -----------------------------------------------------
-- View `General`.`Registro_Consultas`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `General`.`Registro_Consultas` ;
DROP TABLE IF EXISTS `General`.`Registro_Consultas`;
USE `General`;
CREATE OR REPLACE VIEW `General`.`Registro_Consultas` AS
SELECT
`Especialistas`.`idMedicos`,
`Especialistas`.`nombre`,
`Consultas`.`idConsultas`,
`Consultas`.`fecha`,
`Personas`.`idPersonas`,
`Personas`.`nombre` as `pacientes`,
`Consultas`.`motivo`,
`Consultas`.`notas`
FROM `General`.`Consultas`
LEFT JOIN `Pacientes`.`Personas` ON (`Consultas`.`idPersonas` = `Personas`.`idPersonas`)
LEFT JOIN `Hospitales`.`Especialistas` ON (`Consultas`.`idMedicos` = `Especialistas`.`idMedicos`);
USE `Tratamientos` ;

-- -----------------------------------------------------
-- Table `Tratamientos`.`Plan_Tratamientos_Farmacologicos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Tratamientos`.`Plan_Tratamientos_Farmacologicos` ;

CREATE  TABLE IF NOT EXISTS `Tratamientos`.`Plan_Tratamientos_Farmacologicos` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT UNSIGNED NOT NULL ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `medicamento` VARCHAR(100) NOT NULL ,
  `dosis` DOUBLE NOT NULL DEFAULT 1.0 ,
  `unidad` VARCHAR(15) NOT NULL DEFAULT 'Unidades' ,
  `duracion` VARCHAR(100) NULL DEFAULT 'Mienstras continuen los sintomas' ,
  `horario` VARCHAR(100) NULL DEFAULT 'Todos los dias' ,
  `notas` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Medicacion_1` (`idConsultas` ASC) ,
  INDEX `ACTIVO` (`activo` ASC, `idConsultas` ASC) ,
  CONSTRAINT `fk_Medicacion_1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Tabla que guarda los Tratamientos de caracter Farmacologico.';


-- -----------------------------------------------------
-- Table `Tratamientos`.`Plan_Tratamientos_No_Farmacologicos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Tratamientos`.`Plan_Tratamientos_No_Farmacologicos` ;

CREATE  TABLE IF NOT EXISTS `Tratamientos`.`Plan_Tratamientos_No_Farmacologicos` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT UNSIGNED NOT NULL ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `nombre` VARCHAR(100) NOT NULL ,
  `duracion` VARCHAR(100) NULL DEFAULT 'Una ves a la semana' ,
  `descripcion` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Terapias_Consultas1` (`idConsultas` ASC) ,
  INDEX `ACTIVO` (`activo` ASC, `idConsultas` ASC) ,
  CONSTRAINT `fk_Terapias_Consultas1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Tabla que guarda los Tratamientos de caracter No Farmacologi' /* comment truncated */;


-- -----------------------------------------------------
-- Table `Tratamientos`.`Plan_Diagnostico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Tratamientos`.`Plan_Diagnostico` ;

CREATE  TABLE IF NOT EXISTS `Tratamientos`.`Plan_Diagnostico` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT UNSIGNED NOT NULL ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `examen` VARCHAR(50) NOT NULL ,
  `notas` MEDIUMTEXT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Plan_Diagnostico_1` (`idConsultas` ASC) ,
  CONSTRAINT `fk_Plan_Diagnostico_1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Placeholder table for view `Tratamientos`.`Medicacion_Activa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tratamientos`.`Medicacion_Activa` (`idPersonas` INT, `id` INT, `medicamento` INT, `dosis` INT, `unidad` INT, `horario` INT, `notas` INT, `fecha` INT, `duracion` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Tratamientos`.`Tratamientos_Activos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tratamientos`.`Tratamientos_Activos` (`idPersonas` INT, `id` INT, `nombre` INT, `descripcion` INT, `fecha` INT, `duracion` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Tratamientos`.`Examenes_Pedidos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tratamientos`.`Examenes_Pedidos` (`idPersonas` INT, `id` INT, `examen` INT, `notas` INT, `fecha` INT);

-- -----------------------------------------------------
-- View `Tratamientos`.`Medicacion_Activa`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Tratamientos`.`Medicacion_Activa` ;
DROP TABLE IF EXISTS `Tratamientos`.`Medicacion_Activa`;
USE `Tratamientos`;
CREATE OR REPLACE VIEW `Tratamientos`.`Medicacion_Activa` AS
SELECT
`Consultas`.`idPersonas`,
`Plan_Tratamientos_Farmacologicos`.`id`,
`Plan_Tratamientos_Farmacologicos`.`medicamento`,
`Plan_Tratamientos_Farmacologicos`.`dosis`,
`Plan_Tratamientos_Farmacologicos`.`unidad`,
`Plan_Tratamientos_Farmacologicos`.`horario`,
`Plan_Tratamientos_Farmacologicos`.`notas`,
`Plan_Tratamientos_Farmacologicos`.`fecha`,
`Plan_Tratamientos_Farmacologicos`.`duracion`
FROM `Tratamientos`.`Plan_Tratamientos_Farmacologicos`
LEFT JOIN `General`.`Consultas` ON `Plan_Tratamientos_Farmacologicos`.`idConsultas` = `Consultas`.`idConsultas`
WHERE `Plan_Tratamientos_Farmacologicos`.`activo` = 1;

-- -----------------------------------------------------
-- View `Tratamientos`.`Tratamientos_Activos`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Tratamientos`.`Tratamientos_Activos` ;
DROP TABLE IF EXISTS `Tratamientos`.`Tratamientos_Activos`;
USE `Tratamientos`;
CREATE OR REPLACE VIEW `Tratamientos`.`Tratamientos_Activos` AS
SELECT
`Consultas`.`idPersonas`,
`Plan_Tratamientos_No_Farmacologicos`.`id`,
`Plan_Tratamientos_No_Farmacologicos`.`nombre`,
`Plan_Tratamientos_No_Farmacologicos`.`descripcion`,
`Plan_Tratamientos_No_Farmacologicos`.`fecha`,
`Plan_Tratamientos_No_Farmacologicos`.`duracion`
FROM `Tratamientos`.`Plan_Tratamientos_No_Farmacologicos`
LEFT JOIN `General`.`Consultas` ON `Plan_Tratamientos_No_Farmacologicos`.`idConsultas` = `Consultas`.`idConsultas`
WHERE `Plan_Tratamientos_No_Farmacologicos`.`activo` = 1;

-- -----------------------------------------------------
-- View `Tratamientos`.`Examenes_Pedidos`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Tratamientos`.`Examenes_Pedidos` ;
DROP TABLE IF EXISTS `Tratamientos`.`Examenes_Pedidos`;
USE `Tratamientos`;
CREATE  OR REPLACE VIEW `Tratamientos`.`Examenes_Pedidos` AS
SELECT
`Consultas`.`idPersonas`,
`Plan_Diagnostico`.`id`,
`Plan_Diagnostico`.`examen`,
`Plan_Diagnostico`.`notas`,
`Plan_Diagnostico`.`fecha`
FROM `Tratamientos`.`Plan_Diagnostico`
LEFT JOIN `General`.`Consultas` ON `Plan_Diagnostico`.`idConsultas` = `Consultas`.`idConsultas`
WHERE `Plan_Diagnostico`.`activo` = 1;
USE `Hospitales` ;

-- -----------------------------------------------------
-- Table `Hospitales`.`Especialistas_Hospitales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Hospitales`.`Especialistas_Hospitales` ;

CREATE  TABLE IF NOT EXISTS `Hospitales`.`Especialistas_Hospitales` (
  `idHospitales` INT UNSIGNED NOT NULL ,
  `idMedicos` INT UNSIGNED NOT NULL ,
  `fecha_asignacion` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`idHospitales`, `idMedicos`) ,
  INDEX `fk_Hospitales_has_Especialistas_Especialistas1` (`idMedicos` ASC) ,
  INDEX `fk_Hospitales_has_Especialistas_Hospitales1` (`idHospitales` ASC) ,
  CONSTRAINT `fk_Hospitales_has_Especialistas_Hospitales1`
    FOREIGN KEY (`idHospitales` )
    REFERENCES `Hospitales`.`Centros_Medicos` (`idHospitales` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Hospitales_has_Especialistas_Especialistas1`
    FOREIGN KEY (`idMedicos` )
    REFERENCES `Hospitales`.`Especialistas` (`idMedicos` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Tabla que guarda la información de los centros de atención m' /* comment truncated */;


-- -----------------------------------------------------
-- Placeholder table for view `Hospitales`.`Medicos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hospitales`.`Medicos` (`idMedicos` INT, `nombre` INT, `especialidad` INT, `ci` INT, `edad` INT, `sexo` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Hospitales`.`Medicos_Tratantes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hospitales`.`Medicos_Tratantes` (`idPersonas` INT, `Nobre` INT, `Especialidad` INT, `fecha_asignacion` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Hospitales`.`Hospitales`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hospitales`.`Hospitales` (`idHospitales` INT, `nombre` INT, `rif` INT, `telefono` INT, `direccion` INT);

-- -----------------------------------------------------
-- View `Hospitales`.`Medicos`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Hospitales`.`Medicos` ;
DROP TABLE IF EXISTS `Hospitales`.`Medicos`;
USE `Hospitales`;
CREATE OR REPLACE VIEW `Hospitales`.`Medicos` AS
SELECT
`Especialistas`.`idMedicos`,
`Especialistas`.`nombre`,
`Especialistas`.`especialidad`,
CONCAT(`Especialistas`.`ci_tipo`, '-', `Especialistas`.`ci_numero`) AS `ci`,
FLOOR(DATEDIFF(NOW(), `Especialistas`.`fecha_nacimiento`)/365) AS `edad`,
CASE `Especialistas`.`sexo` WHEN 1 THEN 'Masculino' WHEN 2 THEN 'Femenino' ELSE 'Otro' END AS `sexo`
FROM `Hospitales`.`Especialistas`;

-- -----------------------------------------------------
-- View `Hospitales`.`Medicos_Tratantes`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Hospitales`.`Medicos_Tratantes` ;
DROP TABLE IF EXISTS `Hospitales`.`Medicos_Tratantes`;
USE `Hospitales`;
CREATE OR REPLACE VIEW `Hospitales`.`Medicos_Tratantes` AS
SELECT
`Personas`.`idPersonas`,
`Especialistas`.`nombre` AS `Nobre`,
`Especialistas`.`especialidad` AS `Especialidad`,
`Personas_Especialistas`.`fecha_asignacion`
FROM `Pacientes`.`Personas`
RIGHT JOIN `Pacientes`.`Personas_Especialistas` ON `Personas_Especialistas`.`idPersonas` = `Personas`.`idPersonas`
LEFT JOIN `Hospitales`.`Especialistas` ON `Personas_Especialistas`.`idMedicos` = `Especialistas`.`idMedicos`
ORDER BY `Personas`.`idPersonas` ASC;

-- -----------------------------------------------------
-- View `Hospitales`.`Hospitales`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Hospitales`.`Hospitales` ;
DROP TABLE IF EXISTS `Hospitales`.`Hospitales`;
USE `Hospitales`;
CREATE OR REPLACE VIEW `Hospitales`.`Hospitales` AS
SELECT
`Centros_Medicos`.`idHospitales`,
`Centros_Medicos`.`nombre`,
CONCAT(`Centros_Medicos`.`rif_tipo`, '-', `Centros_Medicos`.`rif_numero`, '-', `Centros_Medicos`.`rif_final`) AS `rif`,
CONCAT(`Centros_Medicos`.`telefono_codigo`,'-',`Centros_Medicos`.`telefono_numero`) AS `telefono`,
`Centros_Medicos`.`direccion`
FROM `Hospitales`.`Centros_Medicos`
;
USE `Pacientes` ;

-- -----------------------------------------------------
-- Table `Pacientes`.`Diagnosticos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pacientes`.`Diagnosticos` ;

CREATE  TABLE IF NOT EXISTS `Pacientes`.`Diagnosticos` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT UNSIGNED NOT NULL ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `activo` TINYINT(1) NULL DEFAULT 1 ,
  `fecha_terminacion` TIMESTAMP NULL DEFAULT NULL ,
  `nombre` VARCHAR(50) NOT NULL ,
  `descripcion` MEDIUMTEXT NULL DEFAULT NULL ,
  `pronostico` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Diagnosticos_Consultas1` (`idConsultas` ASC) ,
  CONSTRAINT `fk_Diagnosticos_Consultas1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Tabla con todos los diagnosticos realisados a los pacientes.';


-- -----------------------------------------------------
-- Table `Pacientes`.`Contacto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pacientes`.`Contacto` ;

CREATE  TABLE IF NOT EXISTS `Pacientes`.`Contacto` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idPersonas` INT UNSIGNED NOT NULL ,
  `telefono_codigo` DECIMAL(4,0) NULL DEFAULT NULL ,
  `telefono_numero` DECIMAL(7,0) NULL DEFAULT NULL ,
  `correo_electronico_usuario` VARCHAR(50) NULL DEFAULT NULL ,
  `correo_electronico_dominio` VARCHAR(50) NULL DEFAULT NULL ,
  `direccion` MEDIUMTEXT NULL DEFAULT NULL ,
  `estado` VARCHAR(50) NULL DEFAULT NULL ,
  `ciudad` VARCHAR(100) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Contacto_Personas1` (`idPersonas` ASC) ,
  CONSTRAINT `fk_Contacto_Personas1`
    FOREIGN KEY (`idPersonas` )
    REFERENCES `Pacientes`.`Personas` (`idPersonas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Pacientes`.`Caracteristicas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pacientes`.`Caracteristicas` ;

CREATE  TABLE IF NOT EXISTS `Pacientes`.`Caracteristicas` (
  `idCaracteristicas` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idPersonas` INT UNSIGNED NOT NULL ,
  `ultima_fecha_actualisacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `peso` INT UNSIGNED NULL DEFAULT 0 COMMENT 'medida en Kilogramos' ,
  `talla` INT UNSIGNED NULL DEFAULT 0 COMMENT 'medida en centimetros' ,
  `ocupacion` VARCHAR(50) NULL DEFAULT NULL ,
  `habitos_alimenticios` MEDIUMTEXT NULL DEFAULT NULL ,
  `habitos_psicologicos` MEDIUMTEXT NULL DEFAULT NULL ,
  `estilo_vida` MEDIUMTEXT NULL DEFAULT NULL ,
  `notas` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`idCaracteristicas`, `idPersonas`) ,
  INDEX `fk_Caracteristicas_1` (`idPersonas` ASC) ,
  CONSTRAINT `fk_Caracteristicas_1`
    FOREIGN KEY (`idPersonas` )
    REFERENCES `Pacientes`.`Personas` (`idPersonas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Información extra de un paciente, verción formulario con car' /* comment truncated */;


-- -----------------------------------------------------
-- Placeholder table for view `Pacientes`.`Antecedentes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pacientes`.`Antecedentes` (`id` INT, `idPersonas` INT, `paciente` INT, `ci` INT, `nombre` INT, `descripcion` INT, `fecha` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Pacientes`.`Diagnosticos_Activos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pacientes`.`Diagnosticos_Activos` (`id` INT, `idPersonas` INT, `paciente` INT, `ci` INT, `nombre` INT, `descripcion` INT, `pronostico` INT, `fecha` INT);

-- -----------------------------------------------------
-- Placeholder table for view `Pacientes`.`Pacientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pacientes`.`Pacientes` (`idPersonas` INT, `nombre` INT, `ci` INT, `fecha_nacimiento` INT, `edad` INT, `sexo` INT, `tipo_sangre` INT, `telefono` INT, `direccion` INT, `correo_electronico` INT);

-- -----------------------------------------------------
-- View `Pacientes`.`Antecedentes`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Pacientes`.`Antecedentes` ;
DROP TABLE IF EXISTS `Pacientes`.`Antecedentes`;
USE `Pacientes`;
CREATE OR REPLACE VIEW `Pacientes`.`Antecedentes` AS
SELECT
`Diagnosticos`.`id`,
`Personas`.`idPersonas`,
`Personas`.`nombre` as `paciente`,
CONCAT(`Personas`.`ci_tipo`, '-', `Personas`.`ci_numero`) AS `ci`,
`Diagnosticos`.`nombre`,
`Diagnosticos`.`descripcion`,
`Diagnosticos`.`fecha`
FROM `Pacientes`.`Diagnosticos`
LEFT JOIN `General`.`Consultas` ON `Diagnosticos`.idConsultas = `Consultas`.idConsultas
LEFT JOIN `Pacientes`.`Personas` ON `Consultas`.idPersonas = `Personas`.idPersonas
WHERE `Diagnosticos`.`activo` = 0
ORDER BY `Personas`.`nombre` ASC, `Diagnosticos`.`fecha` DESC;

-- -----------------------------------------------------
-- View `Pacientes`.`Diagnosticos_Activos`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Pacientes`.`Diagnosticos_Activos` ;
DROP TABLE IF EXISTS `Pacientes`.`Diagnosticos_Activos`;
USE `Pacientes`;
CREATE OR REPLACE VIEW `Pacientes`.`Diagnosticos_Activos` AS
SELECT
`Diagnosticos`.`id`,
`Personas`.`idPersonas`,
`Personas`.`nombre` as `paciente`,
CONCAT(`Personas`.`ci_tipo`, '-', `Personas`.`ci_numero`) AS `ci`,
`Diagnosticos`.`nombre`,
`Diagnosticos`.`descripcion`,
`Diagnosticos`.`pronostico`,
`Diagnosticos`.`fecha`
FROM `Pacientes`.`Diagnosticos`
LEFT JOIN `General`.`Consultas` ON `Diagnosticos`.idConsultas = `Consultas`.idConsultas
LEFT JOIN `Pacientes`.`Personas` ON `Consultas`.idPersonas = `Personas`.idPersonas
WHERE `Diagnosticos`.`activo` = 1
ORDER BY `Personas`.`nombre` ASC, `Diagnosticos`.`fecha` DESC;

-- -----------------------------------------------------
-- View `Pacientes`.`Pacientes`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `Pacientes`.`Pacientes` ;
DROP TABLE IF EXISTS `Pacientes`.`Pacientes`;
USE `Pacientes`;
CREATE OR REPLACE VIEW `Pacientes`.`Pacientes` AS
SELECT
`Personas`.`idPersonas`,
`Personas`.`nombre`,
CONCAT(`Personas`.`ci_tipo`, '-', `Personas`.`ci_numero`) AS `ci`,
CONCAT(
 EXTRACT(DAY FROM `Personas`.`fecha_nacimiento`),
 '-',
 EXTRACT(MONTH FROM `Personas`.`fecha_nacimiento`),
 '-',
 EXTRACT(YEAR FROM `Personas`.`fecha_nacimiento`)
 ) as `fecha_nacimiento`,
FLOOR(DATEDIFF(NOW(), `Personas`.`fecha_nacimiento`)/365) AS `edad`,
CASE `Personas`.`sexo` WHEN 1 THEN 'Masculino' WHEN 2 THEN 'Femenino' ELSE 'Otro' END AS `sexo`,
CONCAT(CASE `Personas`.`abo` WHEN 0 THEN 'A' WHEN 1 THEN 'B' WHEN 2 THEN 'AB' WHEN 3 THEN 'O' END, CASE `Personas`.`rh` WHEN 0 THEN '-' WHEN 1 THEN '+' END) AS `tipo_sangre`,
CONCAT(`Contacto`.`telefono_codigo`,'-',`Contacto`.`telefono_numero`) AS `telefono`,
`Contacto`.`direccion`,
CONCAT(`Contacto`.`correo_electronico_usuario`, '@', `Contacto`.`correo_electronico_dominio`) AS `correo_electronico`
FROM `Pacientes`.`Personas`
JOIN `Pacientes`.`Contacto` ON `Personas` .`idPersonas` = `Contacto`.`idPersonas`;
USE `Semiologia`;
