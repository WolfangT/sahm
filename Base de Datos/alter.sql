SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


DELIMITER $$

USE `pacientes`$$
DROP TRIGGER IF EXISTS `pacientes`.`Contacto_BINS` $$

USE `pacientes`$$
CREATE
DEFINER=`wolfang`@`%`
TRIGGER `pacientes`.`Contacto_BINS`
BEFORE INSERT ON `pacientes`.`contacto`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.estado = `General`.CAP_FIRST(NEW.estado);
SET NEW.ciudad = `General`.CAP_FIRST(NEW.ciudad);
END$$

USE `pacientes`$$
DROP TRIGGER IF EXISTS `pacientes`.`Diagnosticos_BINS` $$

USE `pacientes`$$
CREATE
DEFINER=`wolfang`@`%`
TRIGGER `pacientes`.`Diagnosticos_BINS`
BEFORE INSERT ON `pacientes`.`diagnosticos`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
END$$

USE `semiologia`$$
DROP TRIGGER IF EXISTS `semiologia`.`Signos_BINS` $$

USE `semiologia`$$
CREATE
DEFINER=`wolfang`@`%`
TRIGGER `semiologia`.`Signos_BINS`
BEFORE INSERT ON `semiologia`.`signos`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
END$$

USE `semiologia`$$
DROP TRIGGER IF EXISTS `semiologia`.`Sintomas_BINS` $$

USE `semiologia`$$
CREATE
DEFINER=`wolfang`@`%`
TRIGGER `semiologia`.`Sintomas_BINS`
BEFORE INSERT ON `semiologia`.`sintomas`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.nombre = `General`.CAP_FIRST(NEW.nombre);
END$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
