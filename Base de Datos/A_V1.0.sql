SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS `Tratamientos`.`Plan_Diagnostico` ;
CREATE  TABLE IF NOT EXISTS `Tratamientos`.`Plan_Diagnostico` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `idConsultas` INT(10) UNSIGNED NOT NULL ,
  `activo` TINYINT(1) NOT NULL DEFAULT 1 ,
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `examen` VARCHAR(50) NULL DEFAULT NULL ,
  `notas` MEDIUMTEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Plan_Diagnostico_1` (`idConsultas` ASC) ,
  CONSTRAINT `fk_Plan_Diagnostico_1`
    FOREIGN KEY (`idConsultas` )
    REFERENCES `General`.`Consultas` (`idConsultas` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Placeholder table for view `Tratamientos`.`Examenes_Pedidos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tratamientos`.`Examenes_Pedidos` (`id` INT);


USE `Tratamientos`;

-- -----------------------------------------------------
-- View `Tratamientos`.`Examenes_Pedidos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Tratamientos`.`Examenes_Pedidos`;
USE `Tratamientos`;
CREATE  OR REPLACE VIEW `Tratamientos`.`Examenes_Pedidos` AS
SELECT
`Consultas`.`idPersonas`,
`Plan_Diagnostico`.`id`,
`Plan_Diagnostico`.`examen`,
`Plan_Diagnostico`.`notas`,
`Plan_Diagnostico`.`fecha`
FROM `Tratamientos`.`Plan_Diagnostico`
LEFT JOIN `General`.`Consultas` ON `Plan_Diagnostico`.`idConsultas` = `Consultas`.`idConsultas`
WHERE `Plan_Diagnostico`.`activo` = 1;

DELIMITER $$

USE `Tratamientos`$$


CREATE TRIGGER `PlanDiagnostico_AINS` AFTER INSERT ON `Plan_Diagnostico` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Tratamientos',
'Plan_Diagnostico',
CONCAT('Solicitud de Estudio: ', NEW.examen),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NEW.`fecha`
);

END;$$


DELIMITER ;


DELIMITER $$

USE `Tratamientos`$$


CREATE TRIGGER `PlanDiagnostico_AUPD` AFTER UPDATE ON `Plan_Diagnostico` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN

INSERT INTO `General`.`Historial`
(`bd`,
`tabla`,
`descripcion`,
`id`,
`idPersonas`,
`idConsultas`,
`fecha`)
VALUES
(
'Tratamientos',
'Plan_Diagnostico',
CONCAT('Entrega de Estudio: ', NEW.examen),
NEW.`id`,
(SELECT `Consultas`.`idPersonas` From `General`.`Consultas` WHERE `IdConsultas` = NEW.`idConsultas`),
NEW.`idConsultas`,
NOW()
);

END;$$


DELIMITER ;


DELIMITER $$

USE `Tratamientos`$$


CREATE TRIGGER `PlanDiagnostico_BINS` BEFORE INSERT ON `Plan_Diagnostico` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
SET NEW.Examen = `General`.CAP_FIRST(NEW.examen);
END;$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
