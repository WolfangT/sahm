-- -----------------------------------------------------
-- function CAP_FIRST
-- -----------------------------------------------------

USE `General`$$
DROP function IF EXISTS `General`.`CAP_FIRST`$$
CREATE FUNCTION CAP_FIRST (input VARCHAR(255))

RETURNS VARCHAR(255)

DETERMINISTIC

BEGIN
    DECLARE len INT;
    DECLARE i INT;

    SET len   = CHAR_LENGTH(input);
    SET input = LOWER(input);
    SET i = 0;

    WHILE (i < len) DO
        IF (MID(input,i,1) = ' ' OR i = 0) THEN
            IF (i < len) THEN
                SET input = CONCAT(
                    LEFT(input,i),
                    UPPER(MID(input,i + 1,1)),
                    RIGHT(input,len - i - 1)
                );
            END IF;
        END IF;
        SET i = i + 1;
    END WHILE;

    RETURN input;
END;$$

-- -----------------------------------------------------
-- procedure CHQ_EXISTENCIA
-- -----------------------------------------------------

USE `General`$$
DROP procedure IF EXISTS `General`.`CHQ_EXISTENCIA`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CHQ_EXISTENCIA`(IN TABLA VARCHAR(100), IN COL VARCHAR(100), IN ID INT, INOUT R TINYINT(1))
BEGIN

SET @C = 1;

SET @cmd = CONCAT('SELECT count(*) INTO @C FROM ', TABLA, ' WHERE ', COL, ' = ', ID, ';');
PREPARE proc FROM @cmd;
EXECUTE proc;

IF @C > 0 THEN
    SET R = TRUE;
ELSE
    SET R = FALSE;
END IF;

DEALLOCATE PREPARE proc;

END$$
