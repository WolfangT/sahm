#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  main.py
#
#  Copyright 2014 wolfang Torres <wolfang.torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import time, datetime
from datetime import datetime as fecha
from tkinter import ttk, messagebox, Tk, Toplevel

from Libs.libsa import Ventana, Formulario, Bucle, Reporte, _seguro
from Libs.libsa import formato_fecha_usuario, formato_fecha_iso, formato_fecha, formato_fecha_hora
from Libs.libsa import sincronisar_varibles, timedelta_a_iso, timedelta_a_duracion, datos_xml

from Libs.BD_XML import Tabla
from Libs.Widgets.Calendario import Calendar as calendario
import formularios

class Tabla_Estandar(Ventana):
    condicion = False

    def __init__(self, master, con):
        self.master = master
        self.con  = con.copy()

        self._gui()
        self.__tabla__ = self.__gui__.get_object('__tabla__')

        self._tabla()


    @_seguro
    def seleccion(self):
        reg = self.__tabla__.selection()
        filas = []
        for fil in reg:
            n = self.lista_filas.index(fil)
            filas.append(self.tabla[n])
        return tuple(filas)


    def _gui(self):
        Ventana.__init__(self, self.master, self.archivo_gui)


    @_seguro
    def _tabla(self):
        self.tabla.coneccion = (self.con)
        self.tabla.seleccionar_bd(self.nombre_tabla, ('*',), self.condicion)

        for i in self.lista_filas:
            self.__tabla__.delete(i)

        self.lista_filas = []

        for reg in self.tabla:
            valores = []
            for col in self.columnas:
                val = reg[col]

                if isinstance(reg[col], time.struct_time):
                    val = (time.strftime(formato_fecha_hora, reg[col]))

                if isinstance(reg[col], datetime.datetime):
                    val = val.strftime(formato_fecha_hora)

                if isinstance(reg[col], datetime.date):
                    val = val.strftime(formato_fecha)

                if isinstance(reg[col], datetime.time):
                    val = val.strftime(formato_hora)

                if reg[col] is None:
                    val = str()

                valores.append(val)

            id = self.__tabla__.insert('', 'end', values=valores, tags='fila')
            self.lista_filas.append(id)


class Diagnosticos(Tabla_Estandar):
    archivo_gui = 'T_Diagnosticos'

    nombre_tabla='Diagnosticos_Activos'
    nombre_tabla_origen = 'Diagnosticos'
    bd = 'Pacientes'
    columnas = ('nombre', 'fecha', 'descripcion', 'pronostico')
    condicion = {'idPersonas':None}

    def __init__(self, master, con, id):
        self.tabla = Tabla()
        self.lista_filas = []
        self.id = id
        self.condicion = {'idPersonas':id}
        self.master = master
        self.con = con.copy()
        self.con['db'] = self.bd

        Tabla_Estandar.__init__(self, self.master, self.con)

    @_seguro
    def convertir_en_antecedente(self):
        tabla = Tabla()
        tabla.coneccion = (self.con)
        tabla.seleccionar_bd(self.nombre_tabla_origen, ('id','activo'), False)

        ant = self.seleccion()
        for reg in ant:
            tabla += {'id':reg['id'], 'activo':0}

        tabla.actualisar_bd()
        self._tabla()


class Antecedentes(Tabla_Estandar):
    archivo_gui = 'T_Antecedentes'

    nombre_tabla = 'Antecedentes'
    nombre_tabla_origen = 'Diagnosticos'
    bd = 'Pacientes'
    columnas = ('nombre', 'fecha', 'descripcion',)
    condicion = {'idPersonas':None}

    def __init__(self, master, con, id):
        self.tabla = Tabla()
        self.lista_filas = []
        self.id = id
        self.master = master
        self.con = con.copy()
        self.con['db'] = self.bd
        self.condicion = {'idPersonas':id}


        Tabla_Estandar.__init__(self, self.master, self.con)

    @_seguro
    def convertir_en_diagnostico(self):
        tabla = Tabla()
        tabla.coneccion = (self.con)
        tabla.seleccionar_bd(self.nombre_tabla_origen, ('id','activo'), False)

        ant = self.seleccion()
        for reg in ant:
            tabla += {'id':reg['id'], 'activo':1}

        tabla.actualisar_bd()
        self._tabla()


class Tratamientos(Tabla_Estandar):
    archivo_gui = 'T_PlanTratamientos'

    nombre_tabla='Tratamientos_Activos'

    nombre_tabla_origen = 'Plan_Tratamientos_No_Farmacologicos'
    bd = 'Tratamientos'
    columnas = ('nombre', 'descripcion', 'fecha', 'duracion')
    condicion = False

    def __init__(self, master, con, id):
        self.tabla = Tabla()
        self.lista_filas = []
        self.id = id
        self.master = master
        self.con = con.copy()
        self.con['db'] = self.bd
        self.condicion = {'idPersonas':self.id}

        Tabla_Estandar.__init__(self, self.master, self.con)

    @_seguro
    def terminar_tratamiento(self):
        tabla = Tabla()
        tabla.coneccion = (self.con)
        tabla.seleccionar_bd(self.nombre_tabla_origen, ('id','activo'), False)

        ant = self.seleccion()
        for reg in ant:
            tabla += {'id':reg['id'], 'activo':0}

        tabla.actualisar_bd()
        self._tabla()


class Plan_Diagnostico(Tabla_Estandar):
    archivo_gui = 'T_PlanDiagnostico'

    nombre_tabla='Examenes_Pedidos'

    nombre_tabla_origen = 'Plan_Diagnostico'
    bd = 'Tratamientos'
    columnas = ('examen', 'notas', 'fecha')
    condicion = False

    def __init__(self, master, con, id):
        self.tabla = Tabla()
        self.lista_filas = []
        self.id = id
        self.master = master
        self.con = con.copy()
        self.con['db'] = self.bd
        self.condicion = {'idPersonas':self.id}

        Tabla_Estandar.__init__(self, self.master, self.con)

    @_seguro
    def entregar_examen(self):
        tabla = Tabla()
        tabla.coneccion = (self.con)
        tabla.seleccionar_bd(self.nombre_tabla_origen, ('id','activo'), False)

        ant = self.seleccion()
        for reg in ant:
            tabla += {'id':reg['id'], 'activo':0}

        tabla.actualisar_bd()
        self._tabla()


class Medicacion(Tabla_Estandar):
    archivo_gui = 'T_PlanMedicacion'

    nombre_tabla='Medicacion_Activa'
    nombre_tabla_origen = 'Plan_Tratamientos_Farmacologicos'
    bd = 'Tratamientos'
    columnas = ('medicamento', 'dosis', 'unidad', 'horario', 'fecha', 'duracion', 'notas')
    condicion = {'idPersonas':None}

    def __init__(self, master, con, id):
        self.tabla = Tabla()
        self.lista_filas = []
        self.id = id
        self.master = master
        self.con = con.copy()
        self.con['db'] = self.bd
        self.condicion = {'idPersonas':self.id}

        Tabla_Estandar.__init__(self, self.master, self.con)

    @_seguro
    def teminar_medicacion(self):
        tabla = Tabla()
        tabla.coneccion = (self.con)
        tabla.seleccionar_bd(self.nombre_tabla_origen, ('id','activo'), False)

        ant = self.seleccion()
        for reg in ant:
            tabla += {'id':reg['id'], 'activo':0}

        tabla.actualisar_bd()
        self._tabla()


class Medicos_Tratantes(Tabla_Estandar):
    archivo_gui = 'T_MedicosTratantes'

    nombre_tabla = 'Medicos_Tratantes'
    bd = 'Hospitales'
    columnas = ('Nombre', 'Especialidad' )
    condicion = {'idPersonas':None}

    def __init__(self, master, con, id):
        self.tabla = Tabla()
        self.lista_filas = []
        self.id = id
        self.master = master
        self.con = con.copy()
        self.con['db'] = self.bd
        self.condicion = self.condicion = {'idPersonas':self.id}

        Tabla_Estandar.__init__(self, self.master, self.con)


class Histora_Clinica(Tabla_Estandar):
    archivo_gui = 'T_HistoriaClinica'

    nombre_tabla='Historia_Clinica'
    bd = 'General'
    columnas = ('descripcion', 'fecha_evento', 'fecha_consulta', )
    condicion = {'idPersonas':None}

    eventos = Tabla()
    mapeo_tipo_formulario = {
    'Ex_Fisicos':formularios.EX_Fisicos,
    'Ex_Especiales':formularios.EX_Especiales,
    'Ex_ECG':formularios.EX_ECG,
    'Signos':formularios.Signos,
    'Plan_Tratamientos_Farmacologicos':formularios.Plan_Medicacion,
    'Plan_Tratamientos_No_Farmacologicos':formularios.Plan_Tratamiento,
    'Sintomas':formularios.Sintomas,
    'Diagnosticos':formularios.Diagnosticos,
    'Plan_Diagnostico':formularios.Plan_Diagnostico,
    }

    objeto = None

    def __init__(self, master, marco, con, id):
        self.tabla = Tabla()
        self.lista_filas = []
        self.id = id
        self.master = master
        self.marco = marco
        self.con = con.copy()
        self.con['db'] = self.bd

        self.eventos.coneccion = (self.con)

        self.condicion = {'idPersonas':id}

        Tabla_Estandar.__init__(self, self.master, self.con)

    @_seguro
    def selecionar_evento(self, event):
        reg = self.__tabla__.selection()
        n = self.lista_filas.index(reg[0])

        bd = self.tabla[n]['bd']
        tabla = self.tabla[n]['tabla']
        id = self.tabla[n]['id']

        self.eventos.coneccion = ({'db':bd})
        self.eventos.seleccionar_bd(tabla, condicion={'id':id})

        if not self.objeto is None: self.objeto.__vp__.destroy()
        f = self.mapeo_tipo_formulario[tabla]

        self.objeto = f(self.marco, self.con)
        self.objeto.nombre_tabla = tabla
        self.objeto.condicion = {'id':id}
        self.objeto.estado_muestra()
        self.objeto.tabla[0] = self.eventos[0]
        self.objeto.sincronisar('formulario', 0, self.objeto.tabla, 'no_pri','none_a_str')
