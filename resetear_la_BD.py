#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Copyright 2014 Wolfang Torres <wolfang.torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import os, mysql.connector as bd


def crear_sql():
    ddl = []

    ddl += [
    "SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;",
    "SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;",
    "SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';",
    ]

    dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'Base de Datos', 'Tablas.sql'))
    archivo = open(dir)
    sql = archivo.read()

    ddl += sql.split(';')[:-1]

    dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'Base de Datos', 'Triggers.sql'))
    archivo = open(dir)
    sql = archivo.read()

    ddl += sql.split('$$')[:-1]

    dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'Base de Datos', 'Funciones.sql'))
    archivo = open(dir)
    sql = archivo.read()

    ddl += sql.split('$$')[:-1]

    ddl += [
    "SET SQL_MODE=@OLD_SQL_MODE;",
    "SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;",
    "SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;",
    ]

    return ddl

def ejecutar_sql(sql):

    user = input('introdusca la contraseña el usuario:')
    contraseña = input('introdusca la contraseña:')
    host = input('introdusca la ubicación de la base de datos (Ej. 127.0.0.1 si esta en esta computadora):')
    if not bool(host): host = "127.0.0.1"
    datos = {'user':user, 'password':contraseña, 'host':host}
    conecion = bd.connect(**datos)
    cursor = conecion.cursor()

    try:
        for stm in sql:
            cursor.execute(stm)
            print('sql Ejecutado:')
            print(stm)

        conecion.commit()

    except Exception as err:
        conecion.rollback()

        print('Error al ejecutar el sigiente sql')
        print('-------------------------------------------')
        print(stm)
        print('-------------------------------------------')

        raise err

    return 0


print('Reseteo de la base de datos')
print('---------------------------')
print()
print('''Peligro, Borra todos los registros,
Realisar solo si se tiene un respaldo de los datos,
Deberia ser realisado por un admiistrador de bases de datos.
''')
print('---------------------------')
print()
r = input('Desea Continuar? (S/N)')

if r.upper() in ('SI', 'S'):
    sql = crear_sql()
    e = ejecutar_sql(sql)

    print()
    print('-----------------------------------------------------------------')
    print()

    if e == 0:
        print("Reseteo Ejecutado Correctamente")
    else:
        print("No se pudo Realizar el reseteo, Cambios Canceldos")

print()
input('Preciones cualquier tecla para salir')
