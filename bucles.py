#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  main.py
#
#  Copyright 2014 wolfang Torres <wolfang.torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import time, datetime
from datetime import datetime as fecha
from tkinter import ttk, messagebox, Tk, Toplevel

from Libs.libsa import Ventana, Formulario, Bucle, Reporte, _seguro
from Libs.libsa import formato_fecha_usuario, formato_fecha_iso, formato_fecha, formato_fecha_hora
from Libs.libsa import sincronisar_varibles, timedelta_a_iso, datos_xml
from Libs.libsa import Error, ErrorEscritura, ErrorValor, ErrorFormato

from Libs.BD_XML import Tabla
from Libs.Widgets.Calendario import Calendar as calendario

class Bucle_Estandar(Ventana, Bucle):

    def __init__(self, master, con, **funciones):
        self.tabla = Tabla()
        self.con = con.copy()
        self.con['db'] = self.bd
        self.master = master

        self.funciones = funciones
        self.funciones['reiniciar'] = [self.iniciar] + list(self.funciones.get('reiniciar', []))
        self.funciones['buscar'] = [self._buscar] + list(self.funciones.get('buscar', []))

        self.iniciar()
        self._activar()

    @_seguro
    def iniciar(self):
        self.tabla.coneccion = (self.con)
        self.tabla.seleccionar_bd(self.nombre_tabla, self.cols, False)
        Ventana.__init__(self, self.master,  self.archivo_gui)
        Bucle.__init__(self, self.tabla, **self.funciones)
        self._activar()

    @_seguro
    def _buscar(self):
        datos = self.datos
        datos.update(dict([(col, None) for col in self.cols if 'id' in col and datos.get(col, 0) is 0]))

        self.tabla.seleccionar_bd(self.nombre_tabla, self.cols, datos)


class Personas(Bucle_Estandar):

    cols = ('idPersonas','nombre','ci','sexo','telefono')
    nombre_tabla = 'Pacientes'
    bd = 'Pacientes'

    archivo_gui = 'B_Personas'

    def __init__(self, master, con, **funciones):
        Bucle_Estandar.__init__(self, master, con, **funciones)


class Consultas(Bucle_Estandar):

    cols = ('idConsultas','idMedicos','nombre','fecha','idPersonas','pacientes','motivo','notas')
    nombre_tabla = 'Consultas_no_Asistidas'
    bd = 'General'

    archivo_gui = 'B_Consultas'

    def __init__(self, master, con, **funciones):
        Bucle_Estandar.__init__(self, master, con, **funciones)

    @_seguro
    def iniciar(self):
        self.tabla.coneccion = (self.con)
        self.tabla.seleccionar_bd(self.nombre_tabla, self.cols, False)
        Ventana.__init__(self, self.master,  self.archivo_gui)
        self.__gui__.create_variable('int:idPersonas')
        self.__gui__.create_variable('int:idMedicos')
        Bucle.__init__(self, self.tabla, **self.funciones)


class Medicos(Bucle_Estandar):

    cols = ('idMedicos','nombre', 'especialidad', 'ci', )
    nombre_tabla = 'Medicos'
    bd = 'Hospitales'

    archivo_gui = 'B_Medicos'

    def __init__(self, master, con, **funciones):
        Bucle_Estandar.__init__(self, master, con, **funciones)
