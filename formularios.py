#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

#  main.py
#
#  Copyright 2014 wolfang Torres <wolfang.torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#


import time, datetime, Libs.BD_XML as bd
from datetime import datetime as fecha
from tkinter import ttk, messagebox, Tk, Toplevel

from Libs.libsa import Ventana, Formulario, Bucle, Reporte
from Libs.libsa import formato_fecha_usuario, formato_fecha_iso, formato_fecha, formato_fecha_hora
from Libs.libsa import sincronisar_varibles, timedelta_a_iso, timedelta_a_duracion, timedelta_a_fecha, calc_meses, calc_dias, datos_xml
from Libs.libsa import Error, ErrorEscritura, ErrorValor, ErrorFormato, _seguro

from Libs.BD_XML import Tabla
from Libs.Widgets.Calendario import Calendar as calendario


import logging
__registro__ = logging.getLogger(__name__)


class Formulario_Estandar(Ventana, Formulario):
    condicion = False

    def __init__(self, master, con, __estado__=None):
        self.__estado__ = __estado__
        self.master = master
        self.con = con.copy()
        self.con['db'] = self.bd
        self.tabla.coneccion = (self.con)
        self.cnx = None

    def estado_nuevo(self):
        self._tabla()
        self._gui()
        self._formulario()

    def estado_muestra(self):
        self._tabla()
        self._gui()
        self._formulario()
        self._cambiar_estado()

    def _gui(self):
        Ventana.__init__(self, self.master, self.archivo_gui)
        self.__gui__.create_variable('int:idConsultas')
        self.__gui__.create_variable('int:idPersonas')

    def _formulario(self):
        Formulario.__init__(self, self.tabla, guardar=(self._guardar,), reiniciar=(self.estado_nuevo,))

    def _tabla(self):
        self.tabla.seleccionar_bd(tabla=self.nombre_tabla, cols=self.columnas, condicion=self.condicion, cantidad=1)

    def _guardar(self):
        try:
            self.id = id = self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
        except bd.Error:
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        nombre = self.nombre
        self.__estado__.set(self.msj_guardado.format(**locals()))
        return True


class EX_ECG(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Ex_ECG'
    columnas = ('*','id','fecha')
    bd = 'Semiologia'

    nombre = 'El ECG'
    archivo_gui = 'F_ExamenECG'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)


class Plan_Diagnostico(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Plan_Diagnostico'
    columnas = ('*','id','fecha', 'activo')
    bd = 'Tratamientos'

    nombre = 'El Plan Diagnostico'
    archivo_gui = 'F_PlanDiagnostico'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)


class EX_Fisicos(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Ex_Fisicos'
    columnas = ('*','id','fecha')
    bd = 'Semiologia'

    nombre = 'El Examen Fisico'
    archivo_gui = 'F_ExamenFisico'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def estado_nuevo(self):
        self._tabla()
        self._gui()
        self._formulario()

        self.normal_abdomen()
        self.normal_extremidad()
        self.normal_neurologico()
        self.normal_rscsrs()
        self.normal_torax()

    @_seguro
    def normal_rscsrs(self):
        self.__gui__.get_object('rscsrs')['state']='readonly'
        self.__gui__.get_variable('rscsrs').set('Normal')

    @_seguro
    def abnormal_rscsrs(self):
        self.__gui__.get_variable('rscsrs').set('')
        self.__gui__.get_object('rscsrs')['state']='normal'

    @_seguro
    def normal_torax(self):
        self.__gui__.get_object('torax')['state']='readonly'
        self.__gui__.get_variable('torax').set('Normal')

    @_seguro
    def abnormal_torax(self):
        self.__gui__.get_variable('torax').set('')
        self.__gui__.get_object('torax')['state']='normal'

    @_seguro
    def normal_abdomen(self):
        self.__gui__.get_object('abdomen')['state']='readonly'
        self.__gui__.get_variable('abdomen').set('Normal')

    @_seguro
    def abnormal_abdomen(self):
        self.__gui__.get_variable('abdomen').set('')
        self.__gui__.get_object('abdomen')['state']='normal'

    @_seguro
    def normal_extremidad(self):
        self.__gui__.get_object('extremidad')['state']='readonly'
        self.__gui__.get_variable('extremidades').set('Normal')

    @_seguro
    def abnormal_extremidad(self):
        self.__gui__.get_variable('extremidades').set('')
        self.__gui__.get_object('extremidad')['state']='normal'

    @_seguro
    def normal_neurologico(self):
        self.__gui__.get_object('neurologico')['state']='readonly'
        self.__gui__.get_variable('neurologico').set('Normal')

    @_seguro
    def abnormal_neurologico(self):
        self.__gui__.get_variable('neurologico').set('')
        self.__gui__.get_object('neurologico')['state']='normal'


class EX_Especiales(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Ex_Especiales'
    columnas = ('*','id','fecha')
    bd = 'Semiologia'

    nombre = 'El Examen Especial'
    archivo_gui = 'F_ExamenEspecial'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)


class Signos(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Signos'
    columnas = ('*','id','fecha')
    bd = 'Semiologia'

    nombre = 'El Signo Clinico'
    archivo_gui = 'F_ExamenSigno'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)


class Sintomas(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Sintomas'
    columnas = ('*', 'id', 'fecha', 'fecha_terminacion', 'activo')
    bd = 'Semiologia'

    nombre = 'El Sintoma'
    archivo_gui = 'F_Sintomas'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)


class Plan_Medicacion(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Plan_Tratamientos_Farmacologicos'
    columnas = ('*', 'id', 'activo', 'fecha')
    bd = 'Tratamientos'

    nombre = 'La Medicación'
    archivo_gui = 'F_PlanMedicacion'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def _guardar(self):
        try:
            id = self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
        except bd.Error:
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        nombre = self.nombre
        self.__estado__.set(self.msj_guardado.format(**locals()))
        return True


class Plan_Tratamiento(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Plan_Tratamientos_No_Farmacologicos'
    columnas = ('*', 'id', 'activo', 'fecha')
    bd = 'Tratamientos'

    nombre = 'La Terapia'
    archivo_gui = 'F_PlanTratamiento'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def _guardar(self):
        try:
            id = self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
        except bd.Error:
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        nombre = self.nombre
        self.__estado__.set(self.msj_guardado.format(**locals()))
        return True


class Personas(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Personas'
    columnas = ('*','idPersonas')
    bd = 'Pacientes'

    archivo_gui = 'F_Personas'

    nombre = 'El Paciente'
    msj_guardado= 'Paciente <{nombre}> fue registrado con el ID {id}'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def calendario_fecha_nacimiento(self):
        ven = Toplevel(self.master)
        var = self.__gui__.get_variable('fecha_nacimiento')
        cal = calendario(var, ven)
        cal.pack()

    def _formulario(self):
        Formulario.__init__(self, self.tabla, reiniciar=(self._gui, self._tabla, self._formulario), guardar=(self._guardar,))

    def _validar_abo(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == '':
            return True

        if not P.upper() in ('A', 'B', 'AB', 'O'):
            con = 'El grupo ABO solo acepta A, B, AB, O'
            nota = 'Escoja una de las opciones dadas'
            ErrorValor(nombre, con, P, nota, obj=self).mensaje()
            return False

        return True

    def _guardar(self):

        self.tabla[0]['fecha_nacimiento'] = fecha.strptime(self.tabla[0]['fecha_nacimiento'], formato_fecha) if not self.tabla[0]['fecha_nacimiento'] == '' else None

        self.tabla[0]['abo'] = {'A':0, 'B':1, 'AB':2, 'O':3}[self.tabla[0]['abo'].upper()] if not self.tabla[0]['abo'] == '' else None

        try:
            self.id = id = self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
        except bd.Error:
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        nombre = self.nombre
        f = time.strftime(formato_fecha_usuario)

        self.__estado__.set(self.msj_guardado.format(**locals()))
        return True


class PersonasExistente(Formulario_Estandar):

    condicion = False
    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'
    nombre_tabla = 'Personas'
    columnas = ('*','idPersonas')
    bd = 'Pacientes'
    archivo_gui = 'F_Personas_Modificacion'
    nombre = 'El Paciente'
    msj_guardado= 'Paciente <{nombre}> fue registrado con el ID {id}'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def calendario_fecha_nacimiento(self):
        ven = Toplevel(self.master)
        var = self.__gui__.get_variable('fecha_nacimiento')
        cal = calendario(var, ven)
        cal.pack()

    def _validar_ci_numero(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)
        if P == '':
            return True
            #con = '{} es obligatorio'.format(nombre)
            #ErrorValor(nombre, con, P, obj=self).mensaje()
            #return False
        if not P.isdigit():
            esp = 'Numero'
            ErrorEscritura(nombre, esp, P, obj=self).mensaje()
            return False
        if not len(P) in range(1,9):
            con = 'La C.I. no puede tener mas de 8 digitos'
            ErrorValor(nombre, con, P, obj=self).mensaje()
            return False
        return True

    def _formulario(self):
        Formulario.__init__(
            self,
            self.tabla,
            reiniciar=(self._gui, self._tabla, self._formulario),
            guardar=(self._guardar,)
        )

    def _validar_abo(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)
        if P == '':
            return True
        if not P.upper() in ('A', 'B', 'AB', 'O'):
            con = 'El grupo ABO solo acepta A, B, AB, O'
            nota = 'Escoja una de las opciones dadas'
            ErrorValor(nombre, con, P, nota, obj=self).mensaje()
            return False
        return True

    def _escribir(self):
        self.__gui__.get_variable('abo').set(
            {0:'A',1:'B',2:'AB',3:'O'}[self.tabla[0]['abo']] if (
                self.tabla[0]['abo'] is not None) else '')
        self.__gui__.get_variable('fecha_nacimiento').set(
            fecha.strftime(
                self.tabla[0]['fecha_nacimiento'],
                formato_fecha) if (
                    self.tabla[0]['fecha_nacimiento'] is not None)
                else None)

    def _guardar(self):
        self.tabla[0]['fecha_nacimiento'] = ((
            fecha.strptime(
                self.tabla[0]['fecha_nacimiento'],
                formato_fecha))
            if not (self.tabla[0]['fecha_nacimiento'] == '')
            else None)
        self.tabla[0]['abo'] = (
            {'A':0, 'B':1, 'AB':2, 'O':3}[self.tabla[0]['abo'].upper()]
            if not self.tabla[0]['abo'] == '' else None)


class Caracteristicas(Formulario_Estandar):

    nombre_tabla = 'Caracteristicas'
    columnas = ('*', 'idCaracteristicas', 'ultima_fecha_actualisacion')
    bd = 'Pacientes'

    nombre = 'Las caracterisicas'
    archivo_gui = 'F_Caracteristicas'

    condicion = False

    msj_guardado = '{nombre} fuaron actualizadas correctamente'

    nuevo = False

    def __init__(self, master, con, idPersonas, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)
        self.idPersonas = idPersonas
        self.condicion = {'idPersonas':idPersonas}

    def _tabla(self):
        n = self.tabla.seleccionar_bd(self.nombre_tabla, self.columnas, self.condicion, cantidad=1)
        if n <= 0:
            self.nuevo = True

    def _guardar(self):
        try:
            if self.nuevo:
                self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
            else:
                self.tabla.actualisar_bd(llaves=['idPersonas']) if self.cnx is None else self.tabla.actualisar_bd(llaves=['idPersonas'], cnx=self.cnx)
        except bd.Error:
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        nombre = self.nombre
        fecha = time.strftime(formato_fecha_usuario)

        self.__estado__.set(self.msj_guardado.format(**locals()))
        return True


class Contacto(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Contacto'
    columnas = ('*', 'id')
    bd = 'Pacientes'

    nombre = 'La Información de Contacto'
    archivo_gui = 'F_Contacto'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)


class Diagnosticos(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Diagnosticos'
    columnas = ('*', 'id', 'fecha', 'fecha_terminacion', 'activo')
    bd = 'Pacientes'

    nombre = 'El Diagnostico'
    archivo_gui = 'F_Diagnosticos'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)


class Inf_Consulta(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue guardada correctamente'

    nombre_tabla = 'Consultas'
    columnas = ('idConsultas', 'motivo', 'notas')
    bd = 'General'

    nombre = 'La Información de Consulta'
    archivo_gui = 'F_Inf_Consulta'

    def __init__(self, master, con, __estado__=None):
        self.id = None

        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def _guardar(self):
        if self.id is None: return None

        self.tabla[0]['idConsultas'] = self.id
        try:
            self.tabla.actualisar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
        except bd.Error:
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        nombre = self.nombre
        id = self.id
        self.__estado__.set(self.msj_guardado.format(**locals()))

        self.cancelar()
        return True


class Antecedentes(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Diagnosticos'
    columnas = ('*', 'id', 'fecha', 'pronostico', 'fecha')
    bd = 'Pacientes'

    nombre = 'El Antecedente'
    archivo_gui = 'F_Antecedentes'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def estado_nuevo(self):
        self._tabla()
        self._gui()
        self._formulario()
        self.__gui__.create_variable('int:activo')
        self.sincronisar('formulario', 0, self.tabla, 'no_pri', 'none_a_str')

    def calendario_fecha_inicio(self):
        ven = Toplevel(self.master)
        var = self.__gui__.get_variable('fecha_terminacion')
        cal = calendario(var, ven)
        cal.pack()

    def _guardar(self):

        self.tabla[0]['activo'] = 0

        self.tabla[0]['fecha_terminacion'] = fecha.strptime(self.tabla[0]['fecha_terminacion'], formato_fecha) if not self.tabla[0]['fecha_terminacion'] == '' else None

        try:
            self.id = id = self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
        except bd.Error:
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        nombre = self.nombre
        self.__estado__.set(self.msj_guardado.format(**locals()))
        return True


class Ant_rap(Formulario_Estandar):

    archivo_gui = 'F_Ant_rap'

    nombre_tabla = 'Diagnosticos'
    bd = 'Pacientes'
    columnas = ('idConsultas', 'activo', 'nombre', 'descripcion')
    condicion = False

    msj_guardado= '{nombre} fue Programada para el {f} con el ID {id}'

    ant_rap_1 = (
        'Tabaquismo',
        'Alcoholismo',
        'Cafeinismo',
        'Isquemicos',
        'Hipertensivos',
        'Vasculares',
        'Chagas',
        'Neoplasicos',
        'Dislipidemia',
    )

    ant_rap_2 = (
        'Uso de Drogas',
        'Historias Quirurgicas',
    )

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def estado_nuevo(self):
        self._tabla()
        self._gui()
        self._formulario()

    def _guardar(self):
        del self.tabla[0]
        self.idConsultas = self.__gui__.get_variable('idConsultas').get()
        for i in self.ant_rap_1:
            if bool(self.__gui__.get_variable(i).get()):
                self.tabla += (self.idConsultas, 0, i, '')

        for i in self.ant_rap_2:
            if bool(self.__gui__.get_variable(i).get()):
                for ant in self.__gui__.get_variable(i).get().split('\n'):
                    if not bool(ant): continue
                    self.tabla += (self.idConsultas, 0, i, ant)

        if len(self.tabla) > 0:
            try:
                self.id = id = self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
            except bd.Error:
                raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        return True


class CentrosMedicos(Formulario_Estandar):

    condicion = {'idHospitales':1}

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Centros_Medicos'
    columnas = ('*')
    bd = 'Hospitales'

    nombre = 'El Centro de Atención'
    archivo_gui = 'F_CentrosMedicos'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def estado_nuevo(self):
        self._tabla()
        self._gui()
        self._formulario()
        self.sincronisar('formulario', 0, self.tabla, 'none_a_str')

    def _tabla(self):
        n = self.tabla.seleccionar_bd(self.nombre_tabla, self.columnas, self.condicion, cantidad=1)
        if n <= 0:
            self.nuevo = True
        else:
            self.nuevo = False
            self.id = 1

    def _validar_rif_tipo(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == '':
            con = 'El RIF es obligatorio'
            ErrorValor(nombre, con, P, obj=self).mensaje()
            return False

        if not P.upper() in ('J','G'):
            ErrorValor(nombre, 'Solo admite G o J', self.tabla[0]['rif_tipo'], obj=self).mensaje()
            return False

        return True

    def _validar_rif_numero(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == '':
            con = 'El RIF es obligatorio'
            ErrorValor(nombre, con, P, obj=self).mensaje()
            return False

        if not P.isdigit():
            esp = 'Numero'
            ErrorEscritura(nombre, esp, P, obj=self).mensaje()
            return False

        if not len(P) in range(0,9):
            con = 'El RIF no puede tener mas de 8 digitos'
            ErrorValor(nombre, con, P, obj=self).mensaje()
            return False

        if self.tabla.ejecutar_proc('General.CHQ_EXISTENCIA', 'Hospitales.Centros_Medicos', 'rif_numero', P, 0)[3]:
            con = 'El RIF <{}> ya existe'.format(P)
            ErrorValor(nombre, con, P, obj=self).mensaje()
            return False

        return True

    def _validar_rif_final(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == '':
            con = 'El RIF es obligatorio'
            ErrorValor(nombre, con, P, obj=self).mensaje()
            return False

        if not P.isdigit():
            esp = 'Numero'
            ErrorEscritura(nombre, esp, P, obj=self).mensaje()
            return False

        if not len(P) in range(0,2):
            con = 'El ultimo numero debe ser 1 digito'
            ErrorValor(nombre, con, P, obj=self).mensaje()
            return False

        return True

    def _guardar(self):
        self.tabla[0]['idHospitales'] = 1

        try:
            if self.nuevo:
                self.id = id = self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
            else:
                self.tabla.actualisar_bd() if self.cnx is None else self.tabla.actualisar_bd(cnx=self.cnx)
        except bd.Error:
            raise
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        self.__gui__.get_variable('idHospitales').set(1)
        nombre = self.nombre
        self.__estado__.set(self.msj_guardado.format(**locals()))
        return True


class Especialistas(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Especialistas'
    columnas = ('*', 'idMedicos')
    bd = 'Hospitales'

    nombre = 'El Especialista'
    archivo_gui = 'F_Especialistas'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        self.con = con
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def calendario_fecha_nacimiento(self):
        ven = Toplevel(self.master)
        var = self.__gui__.get_variable('fecha_nacimiento')
        cal = calendario(var, ven)
        cal.pack()

    def _validar_usuario(self, d, i, P, s, S, v, V, W):
        nombre = self._obtener_nombre(W)

        if P == '':
            con = 'El Nombre de Usuario es obligatorio'
            ErrorValor(nombre, con, P, obj=self).mensaje()
            return False

        if not len(P) in range(0,17):
            con = ("""El texto no puede ser mayor a 16 caracteres""")
            val = ("""{}... ({} caracteres)""".format(P[:16], len(P)))
            ErrorValor(W, con, val, obj=self).mensaje()
            return False

        #~ if self.tabla.ejecutar_proc('General.CHQ_EXISTENCIA', 'Hospitales.Especialistas', 'Usuario', P, 0)[3]:
            #~ con = 'Un medico con ese Ususario ya esta registrado'.format(P)
            #~ ErrorValor(nombre, con, P, obj=self).mensaje()
            #~ return False

        return True

    def _guardar(self):

        self.tabla[0]['fecha_nacimiento'] = fecha.strptime(self.tabla[0]['fecha_nacimiento'], formato_fecha) if not self.tabla[0]['fecha_nacimiento'] == '' else None

        try:
            self.id = id = self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
        except bd.Error:
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        nombre = self.nombre
        self.__gui__.get_variable('idMedicos').set(id)

        #Crear el usuariotry:
        try:
            import mysql.connector as MYDB
        except ImportError:
            try:
                import MySQLdb.connector as MYDB
            except ImportError:
                MYDB = None

        try:
            cnx = MYDB.connect(**self.con)
            cur = cnx.cursor()
            usuario = self.__gui__.get_variable("Usuario").get()
            contraseña = self.__gui__.get_variable("Contraseña").get()

            cur.execute("""CREATE USER '{usuario}'@'%' IDENTIFIED BY '{contraseña}';""".format(**locals() ) )
            cur.execute("""GRANT EXECUTE, SELECT, INSERT, UPDATE ON `General`.* TO '{usuario}'@'%';""".format(**locals() ) )
            cur.execute("""GRANT SELECT ON `Hospitales`.* TO '{usuario}'@'%';""".format(**locals() ) )
            cur.execute("""GRANT SELECT, INSERT, UPDATE ON `Pacientes`.* TO '{usuario}'@'%';""".format(**locals() ) )
            cur.execute("""GRANT SELECT, INSERT, UPDATE ON `Tratamientos`.* TO '{usuario}'@'%';""".format(**locals() ) )
            cur.execute("""GRANT SELECT, INSERT, UPDATE ON `Semiologia`.* TO '{usuario}'@'%';""".format(**locals() ) )
        except Exception as err:
            __registro__.error(err)
        else:
            cnx.commit()

        finally:
            cnx.close()

        self.__estado__.set(self.msj_guardado.format(**locals()))
        return True


class Consultas(Formulario_Estandar):

    condicion = False

    msj_guardado = '{nombre} fue registrado correctamente con el ID {id}'

    nombre_tabla = 'Consultas'
    columnas = ('*','asistida', 'idConsultas')
    bd = 'General'

    archivo_gui = 'F_Consultas'

    nombre = 'La consulta'
    msj_guardado= '{nombre} fue Programada para el {f} con el ID {id}'

    def __init__(self, master, con, __estado__=None):
        self.tabla = Tabla()
        Formulario_Estandar.__init__(self, master, con, __estado__)

    def _formulario(self):
        Formulario.__init__(self, self.tabla, reiniciar=(self._gui, self._tabla, self._formulario), guardar=(self._guardar,))
        self.__gui__.create_variable('int:idMedicos')
        self.__gui__.create_variable('int:idHospitales')

    def calendario_fecha_inicio(self):
        ven = Toplevel(self.master)
        var = self.__gui__.get_variable('fecha')
        cal = calendario(var, ven)
        cal.pack()

    def estado_nuevo(self):
        self._tabla()
        self._gui()
        self._formulario()
        self.tabla[0]['idHospitales'] = 1
        self.sincronisar('formulario', 0, self.tabla, 'no_pri', 'none_a_str')

    def _guardar(self):
        self.tabla[0]['fecha'] = f = fecha.strptime(self.tabla[0]['fecha'], formato_fecha) if not self.tabla[0]['fecha'] == '' else None

        try:
            self.id = id = self.tabla.insertar_bd() if self.cnx is None else self.tabla.insertar_bd(cnx=self.cnx)
        except bd.Error:
            raise Error('Un error inesperado ha ocurrido, No se puede guardar el registro', obj=self)

        self.__gui__.get_variable('idPersonas').set(id)
        nombre = self.nombre
        self.__estado__.set(self.msj_guardado.format(**locals()))
        return True
